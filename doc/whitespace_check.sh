#!/usr/bin/env sh
GREP_OUTPUT="`grep --color=always -nH '\s\+$' $@`"
if [ "$?" -eq 0 ]
then
    echo "\n-- TRAILING WHITESPACE --\n"
    printf "%s\n" "$GREP_OUTPUT"
    echo "\n-- END TRAILING WHITESPACE --\n"
    echo "Trailing whitespace found, fix it."
    exit 1
fi

