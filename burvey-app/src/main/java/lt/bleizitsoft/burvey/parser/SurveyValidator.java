package lt.bleizitsoft.burvey.parser;

import lt.bleizitsoft.burvey.api.staticobjects.SurveyQuestionType;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Povilas on 5/22/2017.
 */
public class SurveyValidator {
    public static String ValidateAnswers(SurveyTuple surveyTuple) {
        List<QuestionAnswersTuple> questionAnswersTuples = surveyTuple.getQuestionAnswersTuples();
        Map<Integer, List<AnswerFromExcel>> answers = surveyTuple.getAnswers();

        for(Object key : answers.keySet()) {
            List<AnswerFromExcel> answersFromExcel = answers.get(key);

            for(int i = 0; i < answersFromExcel.size(); i++) {
                AnswerFromExcel answerFromExcel = answersFromExcel.get(i);

                int questionNumber = answerFromExcel.getQuestionNumber();
                String answer = answerFromExcel.getAnswer();

                if (questionNumber > questionAnswersTuples.size()) {
                    return "Pateiktas atsakymas į klausimą, kuris neegzistuoja (nėra tokio nr.)";
                }

                Optional<QuestionAnswersTuple> currentQuestionAnswersTuple = questionAnswersTuples.stream().
                        filter(q -> q.getQuestion().getQuestionNumber() == questionNumber).
                        findFirst();

                if(!currentQuestionAnswersTuple.isPresent()) {
                    return "Pateiktas atsakymas į klausimą su numeriu, kuris neegzistuoja";
                }

                QuestionAnswersTuple currentTuple = currentQuestionAnswersTuple.get();

                if("CHECKBOX".equalsIgnoreCase(currentTuple.getQuestion().getQuestionType()) || "MULTIPLECHOICE".equalsIgnoreCase(currentTuple.getQuestion().getQuestionType())) {
                    if(!currentTuple.getOptionList().contains(answer)) {
                        return "Atsakymas į CHECKBOX arba MULTIPLECHOICE tipo klausimą netinkamas.";
                    }
                } else if("SCALE".equalsIgnoreCase(currentTuple.getQuestion().getQuestionType())) {
                    int firstNum;
                    int secondNum;
                    if(answersFromExcel.size() > 1) {
                        return "Į SCALE tipo klausimą pateiktas daugiau negu vienas atsakymo variantas.";
                    }
                    if (Integer.parseInt(currentTuple.getOptionList().get(0)) >= Integer.parseInt(currentTuple.getOptionList().get(1))) {
                        firstNum = Integer.parseInt(currentTuple.getOptionList().get(0));
                        secondNum = Integer.parseInt(currentTuple.getOptionList().get(1));
                    } else {
                        secondNum = Integer.parseInt(currentTuple.getOptionList().get(0));
                        firstNum = Integer.parseInt(currentTuple.getOptionList().get(1));
                    }
                    try {
                        int answerNumber = Integer.parseInt(answer);
                        if(!(firstNum <= answerNumber && secondNum >= answerNumber)) {
                            return "Atsakymas į SCALE tipo klausimą netinkamas (nėra nurodytuose rėžiuose)";
                        }
                    }
                    catch (NumberFormatException ex) {
                        return "Į SCALE tipo klausimą pateiktas atsakymas nėra sveikas skaičius.";
                    }
                }
            }
        }

        return null;
    }

    public static String ValidateSurvey(List<QuestionAnswersTuple> questionAnswersTuples) {
        for (int i = 0; i < questionAnswersTuples.size(); i++) {
            QuestionAnswersTuple currentTuple = questionAnswersTuples.get(i);

            if(SurveyQuestionType.text.equalsIgnoreCase(currentTuple.getQuestion().getQuestionType())) {
                if (currentTuple.getOptionList().size() > 0) {
                    return SurveyQuestionType.text + " tipo klausimas neturi turėti optionList parametrų";
                }
            } else if (SurveyQuestionType.multipleChoice.equalsIgnoreCase(currentTuple.getQuestion().getQuestionType())) {
                if (currentTuple.getOptionList().size() <= 0) {
                    return SurveyQuestionType.multipleChoice + " tipo klausimas turi turėti optionList parametrų";
                }
            } else if (SurveyQuestionType.checkbox.equalsIgnoreCase(currentTuple.getQuestion().getQuestionType())) {
                if (currentTuple.getOptionList().size() <= 1) {
                    return SurveyQuestionType.checkbox + " tipo klausimas turi turėti optionList bent 2 parametrus";
                }
            } else if (SurveyQuestionType.scale.equalsIgnoreCase(currentTuple.getQuestion().getQuestionType())) {
                if (currentTuple.getOptionList().size() != 2) {
                    return SurveyQuestionType.scale + " tipo klausimui nurodytas neteisngas skaičius optionList parametrų";
                }
                try {
                    Integer.parseInt(currentTuple.getOptionList().get(0));
                    Integer.parseInt(currentTuple.getOptionList().get(1));
                }
                catch (NumberFormatException ex) {
                    return SurveyQuestionType.scale + " parametruose optionList nurodyti ne sveikieji skaičiai";
                }
            }
        }
        return null;
    }


}
