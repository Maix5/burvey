package lt.bleizitsoft.burvey.parser;

import java.util.Date;

/**
 * Created by Povilas on 6/1/2017.
 */
public class SurveyHeader {
    private String name;
    private String description;
    private Date validate;
    private boolean isPublic;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getValidate() {
        return validate;
    }

    public void setValidate(Date validate) {
        this.validate = validate;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }
}
