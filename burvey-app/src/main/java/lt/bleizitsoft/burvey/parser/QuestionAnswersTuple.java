package lt.bleizitsoft.burvey.parser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Povilas on 5/10/2017.
 */
public class QuestionAnswersTuple {
    private QuestionFromExcel question = new QuestionFromExcel();
    private List<String> optionList = new ArrayList<String>();

    public QuestionFromExcel getQuestion() {
        return question;
    }

    public void setQuestion(QuestionFromExcel question) {
        this.question = question;
    }

    public  List<String> getOptionList() {
        return optionList;
    }

    public void addOption(String option) {
        this.optionList.add(option);
    }
}
