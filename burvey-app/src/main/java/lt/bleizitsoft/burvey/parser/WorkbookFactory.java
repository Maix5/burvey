package lt.bleizitsoft.burvey.parser;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.DocumentFactoryHelper;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Povilas on 5/10/2017.
 */
public class WorkbookFactory {
    public static Workbook getWorkbook(InputStream inputStream)
            throws IOException {
        Workbook workbook = null;
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

        if (DocumentFactoryHelper.hasOOXMLHeader(bufferedInputStream)) {
            workbook = new XSSFWorkbook(bufferedInputStream);
        } else if (POIFSFileSystem.hasPOIFSHeader(bufferedInputStream)) {
            workbook = new HSSFWorkbook(bufferedInputStream);
        } else {
            throw new IllegalArgumentException("The specified file is not Excel file");
        }

        return workbook;
    }
}
