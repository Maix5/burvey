package lt.bleizitsoft.burvey.parser;

import java.util.List;
import java.util.Map;

/**
 * Created by Povilas on 5/26/2017.
 */
public class SurveyTuple {
    private List<QuestionAnswersTuple> questionAnswersTuples;
    private Map<Integer, List<AnswerFromExcel>> answers;
    private SurveyHeader header = new SurveyHeader();

    public List<QuestionAnswersTuple> getQuestionAnswersTuples() {
        return questionAnswersTuples;
    }

    public void setQuestionAnswersTuples(List<QuestionAnswersTuple> questionAnswersTuples) {
        this.questionAnswersTuples = questionAnswersTuples;
    }

    public Map<Integer, List<AnswerFromExcel>> getAnswers() {
        return answers;
    }

    public void setAnswers(Map<Integer, List<AnswerFromExcel>> answers) {
        this.answers = answers;
    }

    public SurveyHeader getHeader() {
        return header;
    }

    public void setHeader(SurveyHeader header) {
        this.header = header;
    }
}
