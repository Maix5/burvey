package lt.bleizitsoft.burvey.parser;

import lt.bleizitsoft.burvey.usecase.Exceptions.CellValueTypeException;
import org.apache.poi.ss.usermodel.*;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Povilas on 5/9/2017.
 */
public class ImportExcelParser {
    private Workbook workbook;
    private List<QuestionAnswersTuple> questionAnswersTuples = null;
    private Map<Integer, List<AnswerFromExcel>> answers = new HashMap<Integer, List<AnswerFromExcel>>();
    private SurveyTuple surveyTuple = null;
    private Sheet surveySheet;
    private Sheet answersSheet;
    private Sheet headerSheet;

    public ImportExcelParser(InputStream stream) throws IOException {
        workbook = WorkbookFactory.getWorkbook(stream);
        surveySheet = workbook.getSheet("Survey");
        answersSheet = workbook.getSheet("Answer");
        headerSheet = workbook.getSheet("Header");
    }

    public SurveyTuple parseExcelFile() throws CellValueTypeException, ParseException {
        if (surveyTuple != null) {
            return surveyTuple;
        }

        questionAnswersTuples = new ArrayList<QuestionAnswersTuple>();

        parseSurvey();
        parseAnswers();

        surveyTuple = new SurveyTuple();
        surveyTuple.setHeader(parseHeader());
        surveyTuple.setAnswers(answers);
        surveyTuple.setQuestionAnswersTuples(questionAnswersTuples);
        return surveyTuple;
    }

    private SurveyHeader parseHeader() throws ParseException, CellValueTypeException {
        Iterator<Row> headerSheetIterator = headerSheet.iterator();
        SurveyHeader header = new SurveyHeader();

        while (headerSheetIterator.hasNext()) {
            Row nextRow = headerSheetIterator.next();
            Iterator<Cell> cellIterator = nextRow.cellIterator();

            while (cellIterator.hasNext()) {
                Cell nextCell = cellIterator.next();

                switch (nextCell.getStringCellValue()) {
                    case "$name":
                        header.setName(cellIterator.next().getStringCellValue());
                        break;
                    case "$description":
                        header.setDescription(cellIterator.next().getStringCellValue());
                        break;
                    case "$validate":
                        SimpleDateFormat format = new SimpleDateFormat("yyyy.mm.dd");
                        header.setValidate(format.parse(cellIterator.next().getStringCellValue()));
                        break;
                    case "$public":
                        header.setPublic(getBooleanCellValue(cellIterator.next()));
                        break;
                }
            }
        }
        return header;
    }

    private void parseSurvey() throws CellValueTypeException {
        Iterator<Row> surveySheetIterator = surveySheet.iterator();
        int questionNumberIndex = -1;
        int mandatoryIndex = -1;
        int questionIndex = -1;
        int questionTypeIndex = -1;
        int optionsListIndex = -1;

        while (surveySheetIterator.hasNext()) {
            Row nextRow = surveySheetIterator.next();
            Iterator<Cell> cellIterator = nextRow.cellIterator();

            QuestionAnswersTuple rowTuple = null;

            if (questionNumberIndex > -1 && questionIndex > -1 && questionTypeIndex > -1 && optionsListIndex > -1) {
                rowTuple = new QuestionAnswersTuple();
            }

            while (cellIterator.hasNext()) {
                Cell nextCell = cellIterator.next();
                int columnIndex = nextCell.getColumnIndex();

                if (questionNumberIndex < 0 || questionIndex < 0 || questionTypeIndex < 0 || optionsListIndex < 0) {
                    switch (nextCell.getStringCellValue()) {
                        case "$questionNumber":
                            questionNumberIndex = columnIndex;
                            break;
                        case "$mandatory":
                            mandatoryIndex = columnIndex;
                            break;
                        case "$question":
                            questionIndex = columnIndex;
                            break;
                        case "$questionType":
                            questionTypeIndex = columnIndex;
                            break;
                        case "$optionsList":
                            optionsListIndex = columnIndex;
                            break;
                        default:
                            break;
                    }
                } else {

                    if (columnIndex == questionNumberIndex) {
                        rowTuple.getQuestion().setQuestionNumber((int) nextCell.getNumericCellValue());
                    } else if (columnIndex == mandatoryIndex) {
                        Boolean boolCellValue = getBooleanCellValue(nextCell);
                        if (boolCellValue == null) {
                            break;
                        }
                        rowTuple.getQuestion().setMandatory(boolCellValue.booleanValue());
                    } else if (columnIndex == questionIndex) {
                        rowTuple.getQuestion().setQuestion(nextCell.getStringCellValue());
                    } else if (columnIndex == questionTypeIndex) {
                        rowTuple.getQuestion().setQuestionType(nextCell.getStringCellValue());
                    } else if (columnIndex >= optionsListIndex) {
                        String cellValue = getCellValue(nextCell);
                        if (cellValue != null) {
                            rowTuple.getOptionList().add(getCellValue(nextCell));
                        }
                    }
                }
            }
            if (rowTuple != null) {
                if(rowTuple.getQuestion().getQuestionNumber() > 0 && rowTuple.getQuestion().getQuestionType() != null && rowTuple.getQuestion().getQuestion() != null) {
                    questionAnswersTuples.add(rowTuple);
                } else {
                    return;
                }
            }
        }
    }

    private String getCellValue(Cell cell) {
        if (cell.getCellTypeEnum() == CellType.STRING) {
            return cell.getStringCellValue();
        } else if (cell.getCellTypeEnum() == CellType.NUMERIC) {
            return String.valueOf((int)cell.getNumericCellValue());
        }
        return null;
    }

    private void parseAnswers() {
        Iterator<Row> answersSheetIterator = answersSheet.iterator();
        int answerIdIndex = -1;
        int questionNumberIndex = -1;
        int answerIndex = -1;

        while(answersSheetIterator.hasNext()) {
            Row nextRow = answersSheetIterator.next();
            Iterator<Cell> cellIterator = nextRow.cellIterator();

            int answerId = -1;
            int questionNumber = -1;
            List<String> answers = new ArrayList<String>();

            while(cellIterator.hasNext()) {
                Cell nextCell = cellIterator.next();
                int columnIndex = nextCell.getColumnIndex();

                if (answerIdIndex < 0 || questionNumberIndex < 0 || answerIndex < 0) {
                    switch (nextCell.getStringCellValue()) {
                        case "$answerID":
                            answerIdIndex = columnIndex;
                            break;
                        case "$questionNumber":
                            questionNumberIndex = columnIndex;
                            break;
                        case "$answer":
                            answerIndex = columnIndex;
                            break;
                        default:
                            break;
                    }
                }
                else {
                    if (columnIndex == questionNumberIndex) {
                        questionNumber = (int)nextCell.getNumericCellValue();
                    }
                    else if (columnIndex == answerIdIndex) {
                        answerId = (int)nextCell.getNumericCellValue();
                    }
                    else if (columnIndex >= answerIndex) {
                        answers.add(getCellValue(nextCell));
                    }
                }
            }
            if (questionNumber > 0 && answerId > 0 && !answers.isEmpty()) {
                addAnswerToList(answerId, questionNumber, answers);
            }
        }
    }

    private void addAnswerToList(int answerId, int questionNumber, List<String> answer) {
        if (!answers.containsKey(new Integer(answerId))) {
            answers.put(new Integer(answerId), new ArrayList<AnswerFromExcel>());
        }
        for(String a : answer) {
            AnswerFromExcel answerFromExcel = new AnswerFromExcel();
            answerFromExcel.setAnswer(a);
            answerFromExcel.setQuestionNumber(questionNumber);
            answers.get(new Integer(answerId)).add(answerFromExcel);
        }
    }

    private Boolean getBooleanCellValue(Cell booleanCell) throws CellValueTypeException {
        if(CellType.BLANK == booleanCell.getCellTypeEnum()) {
            return null;
        }
        if(CellType.STRING == booleanCell.getCellTypeEnum()) {
            if("YES".equalsIgnoreCase(booleanCell.getStringCellValue())) {
                return true;
            } else if("NO".equalsIgnoreCase(booleanCell.getStringCellValue())) {
                return false;
            }
        } else if(CellType.BOOLEAN == booleanCell.getCellTypeEnum()) {
            return booleanCell.getBooleanCellValue();
        }
        throw new CellValueTypeException();
    }

}
