package lt.bleizitsoft.burvey.parser;

/**
 * Created by Povilas on 5/26/2017.
 */
public class AnswerFromExcel {
    private String answer;
    private int questionNumber;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getQuestionNumber() {
        return questionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        this.questionNumber = questionNumber;
    }
}
