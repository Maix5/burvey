package lt.bleizitsoft.burvey.usecase.createsurveymodule;

import lombok.Getter;
import lombok.Setter;
import lt.bleizitsoft.burvey.api.authCheck.LoggedIn;
import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;
import lt.bleizitsoft.burvey.api.dao.ISurveyDAO;
import lt.bleizitsoft.burvey.api.staticobjects.InfoMessages;
import lt.bleizitsoft.burvey.api.staticobjects.PagePaths;
import lt.bleizitsoft.burvey.api.staticobjects.SurveyQuestionType;
import lt.bleizitsoft.burvey.api.utils.UrlParser;
import lt.bleizitsoft.burvey.entities.*;
import lt.bleizitsoft.burvey.api.log.LogBusinessLogicInterceptorBinding;
import org.omnifaces.util.Faces;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.Serializable;
import java.time.Duration;
import java.util.*;

/**
 * Created by Gintautas.Grisius on 2017-05-18.
 */
@LoggedIn
@Named
@ViewScoped
public class CreateSurveyController implements Serializable {

    private enum CURRENT_ACTION {
        INFO, QUESTIONS, CONFIRMATION, DONE
    }

    @Inject
    private ISurveyDAO surveyDAO;

    @Inject
    private HttpServletRequest httpServletRequest;

    @Inject
    private IAuthenticatedUserHolder authenticatedUserHolder;

    @Getter @Setter
    private Survey survey = new Survey();

    @Getter
    private String questionType;

    @Getter @Setter
    private Question question = new Question();

    @Getter @Setter
    private NumberQuestion numberQuestion = new NumberQuestion();

    @Getter @Setter
    private ArrayList<AnswerOption> answerOptions = new ArrayList<AnswerOption>();

    @Getter
    private Map<String, String> displayStyle = new HashMap<String, String>();

    @Getter @Setter
    private Object dummyAnswer;

    @Getter
    private boolean updatingExisting = false;

    @Getter
    private boolean isEditable = true;

    private CURRENT_ACTION currentAction = CURRENT_ACTION.INFO;
    public boolean isCurrentAction(CURRENT_ACTION action) {
        return currentAction == action;
    }

    @PostConstruct
    private void init() {

        displayStyle.put("step1", "selected");
        displayStyle.put("step2", "disabled");
        displayStyle.put("step3", "disabled");

        List<String> queryParams = UrlParser.getBaseUrl(httpServletRequest);
        if(queryParams.size() >= 2 && queryParams.get(0).equals("access_link")) {
            survey = surveyDAO.readByColumnAccessLink(queryParams.get(1)).orElse(null);
            if (survey == null || (!authenticatedUserHolder.isAdministrator() &&
                    survey.getOwner().getId() != authenticatedUserHolder.getAuthenticatedUserProfile().getId())) {
                try {
                    InfoMessages.setErrorMessage("Tokia apklausa neegzistuoja!");

                    FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                    Faces.redirect("index");
                } catch (IOException e) {

                }
            }
            checkSurveyEditable();
            updatingExisting = true;
            return;
        }
        survey.setQuestionList(new ArrayList<>());
        survey.setOwner(authenticatedUserHolder.getAuthenticatedUserProfile());
        survey.setIsActive(true);
    }

    public void nextPage() {
        switch (currentAction) {
            case INFO:
                currentAction = CURRENT_ACTION.QUESTIONS;
                survey.setExpirationDate(addTimeFromUTC(survey.getExpirationDate()));
                displayStyle.put("step1", "done");
                displayStyle.put("step2", "selected");
                break;
            case QUESTIONS:
                currentAction = CURRENT_ACTION.CONFIRMATION;
                displayStyle.put("step2", "done");
                displayStyle.put("step3", "selected");
                break;
            case CONFIRMATION:
                break;
        }
    }

    public void previousPage() {
        switch (currentAction) {
            case INFO:
                break;
            case QUESTIONS:
                currentAction = CURRENT_ACTION.INFO;
                survey.setExpirationDate(setTimeToUTC(survey.getExpirationDate()));
                displayStyle.put("step1", "selected");
                displayStyle.put("step2", "done");
                break;
            case CONFIRMATION:
                currentAction = CURRENT_ACTION.QUESTIONS;
                displayStyle.put("step2", "selected");
                displayStyle.put("step3", "done");
                break;
        }
    }

    @LogBusinessLogicInterceptorBinding
    @Transactional
    public void saveSurvey() {
        if (survey.getQuestionList().size() == 0) {
            InfoMessages.setErrorMessage("Įveskite bent vieną klausimą!");
            return;
        }

        if (updatingExisting) {
            InfoMessages.setInfoMessage("Apklausa sėkmingai atnaujinta!");
            surveyDAO.update(survey);
            currentAction = CURRENT_ACTION.DONE;
            return;
        }

        survey.setQuestionsPerPage(5);
        survey.setActivationDate(new Date());
        survey.setAccessLink(UUID.randomUUID().toString());
        surveyDAO.create(survey);

        currentAction = CURRENT_ACTION.DONE;
        InfoMessages.setInfoMessage("Apklausa sėkmingai sukurta!");
    }

    public void addNewQuestion(int type) {
        question = new Question();
        changeQuestionType(type);
        question.setQuestionType(questionType);
        question.setSurvey(survey);
        question.setOrderNr(survey.getQuestionList().size()+1);

        if (questionType.equals(SurveyQuestionType.scale)) {
            numberQuestion = new NumberQuestion();
            numberQuestion.setQuestion(question);
        } else {
            answerOptions = new ArrayList<AnswerOption>();
            addAnswerOption();
            addAnswerOption();
        }
    }

    public void addAnswerOption() {
        answerOptions.add(createAnswerOption());
    }

    public void removeAnswerOption(int index) {
        answerOptions.remove(index);
    }

    public AnswerOption createAnswerOption() {
       AnswerOption answerOption = new AnswerOption();
       answerOption.setQuestion(question);

       return answerOption;
    }

    public void addQuestion() {
        switch (questionType) {
            case SurveyQuestionType.multipleChoice:
                question.setAnswerOptionList(answerOptions);
                break;
            case SurveyQuestionType.checkbox:
                question.setAnswerOptionList(answerOptions);
                break;
            case SurveyQuestionType.scale:
                if (numberQuestion.getMinimalRange() >= numberQuestion.getMaximumRange()) {
                    InfoMessages.setErrorMessage("Minimalus rėžis negali būti didesnis arba lygus maksimaliam!", "modalMessages");
                    return;
                }
                question.setNumberQuestion(numberQuestion);
                break;
        }
        survey.getQuestionList().add(question);
        question = new Question();
        RequestContext.getCurrentInstance().execute("$('.modalPseudoClass').hide();$('.modal-backdrop').remove(); " +
                "$('body').css('overflow', 'auto');");
    }

    public void changeQuestionType(int type) {
        switch (type) {
            case 1:
                questionType = SurveyQuestionType.multipleChoice;
                break;
            case 2:
                questionType = SurveyQuestionType.checkbox;
                break;
            case 3:
                questionType = SurveyQuestionType.text;
                break;
            case 4:
                questionType = SurveyQuestionType.scale;
                break;
        }
    }

    public void questionAction(String action, int currentIndex) {
        switch (action) {
            case "up":
                if (currentIndex < 1) return;
                survey.getQuestionList().get(currentIndex).setOrderNr(currentIndex);
                survey.getQuestionList().get(currentIndex-1).setOrderNr(currentIndex+1);
                Collections.swap(survey.getQuestionList(), currentIndex, currentIndex-1);
                break;
            case "down":
                if (currentIndex == survey.getQuestionList().size()-1) return;
                survey.getQuestionList().get(currentIndex).setOrderNr(currentIndex+2);
                survey.getQuestionList().get(currentIndex+1).setOrderNr(currentIndex+1);
                Collections.swap(survey.getQuestionList(), currentIndex, currentIndex+1);
                break;
            case "delete":
                survey.getQuestionList().remove(currentIndex);
                break;
        }

    }

    public String getSurveyAddress() {
        return PagePaths.BASE_PATH + PagePaths.ANSWER_SURVEY_LINK + "?access_link=" + survey.getAccessLink();
    }

    private void checkSurveyEditable() {
        for (Question question : survey.getQuestionList()) {
            if (question.getUserAnswerList().size() != 0)
            {
                isEditable = false;
                return;
            }
        }
    }

    //adds time from utc to current time zone (+2/+3)
    private Date addTimeFromUTC(Date currentDate) {
        return Date.from(currentDate.toInstant().plus(Duration.ofHours(getHourDiffBetweenUTC())));
    }

    private Date setTimeToUTC(Date currentDate) {
        return Date.from(currentDate.toInstant().minus(Duration.ofHours(getHourDiffBetweenUTC())));
    }

    private int getHourDiffBetweenUTC() {
        return 3;
        //TimeZone.getTimeZone("Europe/Vilnius").getOffset(Calendar.getInstance().getTimeInMillis());
    }
}
