package lt.bleizitsoft.burvey.usecase.usercontrolmodule;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;
import lt.bleizitsoft.burvey.api.log.LogBusinessLogicInterceptorBinding;
import lt.bleizitsoft.burvey.api.optLock.HandleOptLock;
import lt.bleizitsoft.burvey.api.staticobjects.InfoMessages;
import lt.bleizitsoft.burvey.api.staticobjects.PagePaths;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by marius on 5/23/17.
 */
@Named
@ViewScoped
@Slf4j
public class AuthenticatorUseCaseController implements Serializable{

    @Getter @Setter
    private String userEmailAddress;
    @Getter @Setter
    private String userPassword;

    private String originalRequestURI;

    @Inject
    private Provider<IAuthenticatedUserHolder> authenticatedUserHolderProvider;

    @PostConstruct
    public void init() {
        originalRequestURI = Faces.getRequestURIWithQueryString();
//        if (originalRequestURI == null ||
//                originalRequestURI.matches(".*/login\\.xhtml")) {
//            //Address after successful login.
//            originalRequestURI = Faces.getRequestBaseURL();
//        }
    }

    @HandleOptLock
    @LogBusinessLogicInterceptorBinding
    public void loginAndRedirect() {
        // Detaches old session.
        Faces.invalidateSession();
        // Creates new session.
        Faces.getSession(true);

        if (userEmailAddress == "" || userPassword == "") {
            InfoMessages.setErrorMessage("Neįvestas el. paštas arba slaptažodis");
            return;
        }

        try {
            authenticatedUserHolderProvider.get().initUser(
                    userEmailAddress, userPassword);

            if (authenticatedUserHolderProvider.get().isAnonymousUser()) {
                InfoMessages.setErrorMessage("Neteisingas el. paštas arba slaptažodis");
                return;
            }

            if (authenticatedUserHolderProvider.get().getAuthenticatedUserProfile().getBlocked()) {
                InfoMessages.setErrorMessage("Vartotojas užblokuotas");
                Faces.invalidateSession();
                Faces.getSession(true);
                return;
            }
        }catch (IllegalArgumentException e){
            InfoMessages.setErrorMessage("Neteisingas el. paštas arba slaptažodis");
            return;
        }
        finally {
            userPassword = null;
        }

        try {
            Faces.redirect(PagePaths.INDEX_LINK);
        } catch (IOException e) {
            log.error("Authenticator.loginAndRedirect(): ", e);
        }
    }

    @LogBusinessLogicInterceptorBinding
    public void logoutAndRedirect() {
        // call Faces.logout() not necessary.
        Faces.invalidateSession();
        // Creates new session.
        Faces.getSession(true);
        try {
            Faces.redirect(Faces.getRequestBaseURL());
        } catch (IOException e) {
            log.error("Authenticator.logoutAndRedirect(): ", e);
        }
    }

}
