package lt.bleizitsoft.burvey.usecase.usercontrolmodule;

import lombok.Getter;
import lombok.Setter;
import lt.bleizitsoft.burvey.api.authCheck.LoggedIn;
import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;
import lt.bleizitsoft.burvey.api.dao.IUserProfileDAO;
import lt.bleizitsoft.burvey.api.staticobjects.InfoMessages;
import lt.bleizitsoft.burvey.authentification.crypto.BCrypt;
import lt.bleizitsoft.burvey.dao.sync.UserProfileDAO;
import lt.bleizitsoft.burvey.entities.UserProfile;
import lt.bleizitsoft.burvey.api.log.LogBusinessLogicInterceptorBinding;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import java.io.Serializable;

/**
 * Created by Albe on 4/30/2017.
 */
@LoggedIn
@Model
public class UserProfileManagmentController implements Serializable {

    @Getter
    @Setter
    private UserProfile profile;

    private String oldEmail;

    @Getter
    @Setter
    private String newPass;

    @Getter
    @Setter
    private String oldPass;

    @Getter
    @Setter
    private String repeatNewPass;

    @Inject
    private IUserProfileDAO userProfileDAO;

    @Inject
    @Getter
    private IAuthenticatedUserHolder authenticatedUserHolder;

    @PostConstruct
    public void init() {

        profile = authenticatedUserHolder.getAuthenticatedUserProfile();
        oldEmail = profile.getCandidateInfo().getEmailAddress();
    }

    @LogBusinessLogicInterceptorBinding
    @Transactional
    public void changeUserProfile() {
        if (!profile.getCandidateInfo().getEmailAddress().equals(oldEmail)) {
            InfoMessages.setErrorMessage("Elektroninis paštas negali būti keičiamas");
            return;
        }

        if (!newPass.isEmpty() || !oldPass.isEmpty() || !repeatNewPass.isEmpty()) {

            if (oldPass.isEmpty()){
                InfoMessages.setErrorMessage("Neįvestas senas slaptažodis");
                return;
            }

            if (newPass.isEmpty()) {
                InfoMessages.setErrorMessage("Neįvestas naujas slaptažodis");
                return;
            }

            if (repeatNewPass.isEmpty()){
                InfoMessages.setErrorMessage("Pakartokite naują slaptažodį");
                return;
            }

            if (!BCrypt.checkpw(oldPass, profile.getHashedPassword())) {
                InfoMessages.setErrorMessage("Blogai įvestas senas slaptažodis");
                return;
            }

            if (newPass.length()<6){
                InfoMessages.setErrorMessage("Naują slaptažodį turi sudaryti bent 6 simboliai");
            }

            if (!newPass.equals(repeatNewPass)) {
                InfoMessages.setErrorMessage("Slaptažodžiai neatitinka");
                return;
            }

            profile.setHashedPassword(BCrypt.hashpw(newPass, BCrypt.gensalt()));
        }
        InfoMessages.setInfoMessage("Naudotojo informacija atnaujinta");
        userProfileDAO.update(profile);
        authenticatedUserHolder.updateAuthenticatedUserProfile(oldEmail);
    }
}

