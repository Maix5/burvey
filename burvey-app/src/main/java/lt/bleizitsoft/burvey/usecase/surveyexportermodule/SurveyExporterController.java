package lt.bleizitsoft.burvey.usecase.surveyexportermodule;

import lt.bleizitsoft.burvey.api.authCheck.LoggedIn;
import lt.bleizitsoft.burvey.api.dao.IUserAnswerDAO;
import lt.bleizitsoft.burvey.api.staticobjects.InfoMessages;
import lt.bleizitsoft.burvey.api.staticobjects.SurveyQuestionType;
import lt.bleizitsoft.burvey.entities.AnswerOption;
import lt.bleizitsoft.burvey.entities.Question;
import lt.bleizitsoft.burvey.entities.Survey;
import lt.bleizitsoft.burvey.entities.UserAnswer;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.omnifaces.util.Faces;

import javax.enterprise.inject.Model;
import javax.inject.Inject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Gintautas.Grisius
 */
@LoggedIn
@Model
public class SurveyExporterController implements Serializable {

    @Inject
    private IUserAnswerDAO userAnswerDAO;

    private Survey survey;

    private final String fileName = "survey_export.xlsx";

    private final String[] headerSheetTitles = {"$name", "$description", "$validate", "$public"};
    private final String[] surveySheetTitles = {"$questionNumber", "$mandatory", "$question",
            "$questionType", "$optionsList"};
    private final String[] answerSheetTitles = {"$answerID", "$questionNumber", "$answer"};

    public void exportToExcel(Survey survey) {
        if (survey == null) return;
        this.survey = survey;
        survey.getQuestionList().sort((x, y) -> x.getOrderNr().compareTo(y.getOrderNr()));

        try {
            Workbook workbook = new XSSFWorkbook();
            createHeaderSheet(workbook);
            createSurveySheet(workbook);
            createAnswerSheet(workbook);

            File file = new File(fileName);
            FileOutputStream fileOut = new FileOutputStream(file);
            workbook.write(fileOut);
            fileOut.close();
            workbook.close();
            Faces.sendFile(file, true);
            file.deleteOnExit();
        } catch (IOException e) {
            InfoMessages.setErrorMessage("Duomenų eksportas nepavyko");
            return;
        }


    }

    private void createHeaderSheet(Workbook workbook) {
        Sheet headerSheet = workbook.createSheet("Header");
        headerSheet.setColumnWidth(0, 256 * 20);
        headerSheet.setColumnWidth(1, 256 * 20);
        headerSheet.setColumnWidth(2, 256 * 20);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");

        Row titleRow = headerSheet.createRow((short) 0);
        Cell cellA1 = titleRow.createCell((short) 0);
        Cell cellB1 = titleRow.createCell((short) 1);
        cellA1.setCellValue(headerSheetTitles[0]);
        cellB1.setCellValue(survey.getTitle());

        Row descriptionRow = headerSheet.createRow((short) 1);
        Cell cellA2 = descriptionRow.createCell((short) 0);
        Cell cellB2 = descriptionRow.createCell((short) 1);
        cellA2.setCellValue(headerSheetTitles[1]);
        cellB2.setCellValue(survey.getDescription());

        Row validateRow = headerSheet.createRow((short) 2);
        Cell cellA3 = validateRow.createCell((short) 0);
        Cell cellB3 = validateRow.createCell((short) 1);
        cellA3.setCellValue(headerSheetTitles[2]);
        cellB3.setCellValue(sdf.format(survey.getExpirationDate()));

        Row publicRow = headerSheet.createRow((short) 3);
        Cell cellA4 = publicRow.createCell((short) 0);
        Cell cellB4 = publicRow.createCell((short) 1);
        cellA4.setCellValue(headerSheetTitles[3]);
        cellB4.setCellValue(survey.getIsPublic() == true ? "YES" : "NO");

    }

    private void createSurveySheet(Workbook workbook) {
        Sheet surveySheet = workbook.createSheet("Survey");
        surveySheet.setColumnWidth(0, 256 * 20);
        surveySheet.setColumnWidth(1, 256 * 20);
        surveySheet.setColumnWidth(2, 256 * 30);
        surveySheet.setColumnWidth(3, 256 * 20);

        Row titleRow = surveySheet.createRow((short) 0);
        for (int i = 0; i < surveySheetTitles.length; i++) {
            Cell cell = titleRow.createCell((short) i);
            cell.setCellValue(surveySheetTitles[i]);
        }

        for (int i = 0; i < survey.getQuestionList().size(); i++) {
            Question question = survey.getQuestionList().get(i);
            Row row = surveySheet.createRow((short) i + 1);
            row.createCell((short) 0).setCellValue(question.getOrderNr());
            row.createCell((short) 1).setCellValue(question.getRequired() == true ? "YES" : "NO");
            row.createCell((short) 2).setCellValue(question.getContent());
            row.createCell((short) 3).setCellValue(question.getQuestionType());

            if (question.getQuestionType().equals(SurveyQuestionType.scale)) {
                row.createCell((short) 4).setCellValue(question.getNumberQuestion().getMinimalRange());
                row.createCell((short) 5).setCellValue(question.getNumberQuestion().getMaximumRange());

            } else if (question.getQuestionType().equals(SurveyQuestionType.checkbox) ||
                    question.getQuestionType().equals(SurveyQuestionType.multipleChoice)) {

                for (int j = 0; j < question.getAnswerOptionList().size(); j++) {
                    row.createCell((short) j + 4).setCellValue(question.getAnswerOptionList().get(j).getContent());
                    surveySheet.setColumnWidth(j + 4, 256 * 20);
                }
            }
        }
    }

    private void createAnswerSheet(Workbook workbook) {
        int answerId = 0;
        int lastAnswerGroup = 0;
        Sheet answerSheet = workbook.createSheet("Answer");
        answerSheet.setColumnWidth(0, 256 * 20);
        answerSheet.setColumnWidth(1, 256 * 20);
        answerSheet.setColumnWidth(2, 256 * 20);

        Row titleRow = answerSheet.createRow((short) 0);
        for (int i = 0; i < answerSheetTitles.length; i++) {
            Cell cell = titleRow.createCell((short) i);
            cell.setCellValue(answerSheetTitles[i]);
        }

        List<UserAnswer> userAnswers = userAnswerDAO.readAll().stream()
                .filter(userAnswer -> userAnswer.getQuestion().getSurvey().getId().equals(survey.getId()))
                .sorted((userAnswer1, userAnswer2) ->
                        userAnswer1.getQuestion().getId().compareTo(
                                userAnswer2.getQuestion().getId()))
                .sorted((userAnswer1, userAnswer2) ->
                        userAnswer1.getUserSurveyAnswerGroup().getId().compareTo(
                                userAnswer2.getUserSurveyAnswerGroup().getId()))
                .collect(Collectors.toList());


        for (int i = 0; i < userAnswers.size(); i++) {
            UserAnswer userAnswer = userAnswers.get(i);

            if (lastAnswerGroup != userAnswer.getUserSurveyAnswerGroup().getId()) {
                lastAnswerGroup = userAnswer.getUserSurveyAnswerGroup().getId();
                answerId++;
            }

            Row row = answerSheet.createRow((short) i + 1);
            row.createCell((short) 0).setCellValue(answerId);
            row.createCell((short) 1).setCellValue(userAnswer.getQuestion().getOrderNr());

            switch (userAnswer.getQuestion().getQuestionType()) {
                case SurveyQuestionType.text:
                    row.createCell((short) 2).setCellValue(userAnswer.getUserAnswerText().getContentText());
                    break;
                case SurveyQuestionType.scale:
                    row.createCell((short) 2).setCellValue(userAnswer.getUserAnswerNumber().getContentNumber());
                    break;
                case SurveyQuestionType.multipleChoice:
                    row.createCell((short) 2).setCellValue(userAnswer.getQuestion()
                            .getAnswerOptionList().indexOf(userAnswer.getUserSingleAnswerOption().getAnswerOption()) + 1);
                    break;
                case SurveyQuestionType.checkbox:

                    for (int j = 0; j < userAnswer.getUserMultipleChoiceAnswerOption().getAnswerOptionList().size(); j++) {
                        AnswerOption answerOption = userAnswer.getUserMultipleChoiceAnswerOption()
                                .getAnswerOptionList().get(j);

                        row.createCell((short) 2 + j).setCellValue(userAnswer.getQuestion()
                                .getAnswerOptionList().indexOf(answerOption) + 1);
                    }
                    break;
            }
        }
    }
}
