package lt.bleizitsoft.burvey.usecase.surveyreportmodule;

import lt.bleizitsoft.burvey.entities.AnswerOption;
import lt.bleizitsoft.burvey.entities.Question;
import lt.bleizitsoft.burvey.entities.UserAnswer;
import lt.bleizitsoft.burvey.entities.UserSingleAnswerOption;
import org.primefaces.model.chart.PieChartModel;

import javax.enterprise.inject.Model;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Albe on 2017-05-27.
 */
@Model
public class PieChartGraphModel implements Serializable{

    private PieChartModel model;

    public PieChartModel getModel(Question question) {

        List<AnswerOption> optionList = question.getAnswerOptionList();
        List<UserAnswer> resultsList = question.getUserAnswerList();
        int answersCount = 0;
        Map<Integer, Integer> resultsMap = new HashMap<>();
        Map<Integer,AnswerOption> ansOpMap = new HashMap<>();

        for (AnswerOption opt : optionList) {
            ansOpMap.put(opt.getId(),opt);
            resultsMap.put(opt.getId(),0);
        }

        for (UserAnswer ans : resultsList) {
            UserSingleAnswerOption oneUserAnswers = ans.getUserSingleAnswerOption();
            int id = oneUserAnswers.getAnswerOption().getId();
            if (resultsMap.containsKey(id)){
                resultsMap.put(id,resultsMap.get(id)+1);
                answersCount++;
            }

        }

        model = new PieChartModel();
        for (Map.Entry<Integer,Integer> kv : resultsMap.entrySet()) {

            String modelLabel = ansOpMap.get(kv.getKey()).getContent();
            model.set(modelLabel,kv.getValue());
        }
        model.setExtender("ext");
        model.setShowDataLabels(true);
        model.setLegendPosition("e");
        return model;
    }
}
