package lt.bleizitsoft.burvey.usecase.surveyimportermodule;

import lombok.Getter;
import lombok.Setter;
import lt.bleizitsoft.burvey.api.authCheck.LoggedIn;
import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;
import lt.bleizitsoft.burvey.api.dao.*;
import lt.bleizitsoft.burvey.api.log.LogBusinessLogicInterceptorBinding;
import lt.bleizitsoft.burvey.api.staticobjects.InfoMessages;
import lt.bleizitsoft.burvey.entities.Survey;
import lt.bleizitsoft.burvey.parser.*;
import lt.bleizitsoft.burvey.usecase.Exceptions.AnswersGatheringException;
import lt.bleizitsoft.burvey.usecase.Exceptions.CellValueTypeException;

import javax.enterprise.inject.Model;
import javax.inject.Inject;
import javax.servlet.http.Part;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by Albe on 2017-05-11.
 */
@Model
@LoggedIn
public class SurveyImporterController {

    @Getter
    @Setter
    private Part file;

    @Getter
    @Setter
    private Boolean isActive;

    @Getter
    private boolean uploadStarted = false;

    @Inject
    private ISurveyDAO surveyDAO;
    @Inject
    private IQuestionDAO questionDAO;
    @Inject
    private IAnswerOptionDAO answerOptionDAO;
    @Inject
    private IUserSurveyAnswerGroupDAO userSurveyAnswerGroupDAO;
    @Inject
    private IUserAnswerDAO userAnswerDAO;
    @Inject
    private IUserAnswerTextDAO userAnswerTextDAO;
    @Inject
    private IUserAnswerNumberDAO userAnswerNumberDAO;
    @Inject
    private IUserMultipleChoiceAnswerOptionDAO userMultipleChoiceAnswerOptionDAO;
    @Inject
    private IUserSingleAnswerOptionDAO userSingleAnswerOptionDAO;
    @Inject
    private IAuthenticatedUserHolder authenticatedUserHolder;
    @Inject
    private ISurveyFlusher surveyFlusher;

    @LogBusinessLogicInterceptorBinding
    public void save() {
        try (InputStream input = file.getInputStream()) {
            System.out.println(input);
        }
        catch (IOException e) {
            InfoMessages.setErrorMessage("Nepavyko nuskaityti failo!");
        } catch(IllegalArgumentException e) {
            InfoMessages.setErrorMessage("Pasirinktas failas nėra xlsx formato!");
        }

    }

    @LogBusinessLogicInterceptorBinding
    @Transactional
    public void uploadSurvey()
    {
        try (InputStream input = file.getInputStream()) {
            uploadStarted = true;
            ImportExcelParser parser = new ImportExcelParser(input);
            SurveyTuple surveyTuple = parser.parseExcelFile();
            List<QuestionAnswersTuple> questionAnswersTupleList = surveyTuple.getQuestionAnswersTuples();
            String errorMessage = SurveyValidator.ValidateSurvey(questionAnswersTupleList);
            if (errorMessage != null) {
                InfoMessages.setErrorMessage(errorMessage);
                throw new AnswersGatheringException();
            }

            errorMessage = saveSurvey(surveyTuple);
            if (errorMessage != null) {
                InfoMessages.setErrorMessage(errorMessage);
                throw new AnswersGatheringException();
            }
            InfoMessages.setInfoMessage("Apklausa įkelta");
        }
        catch (IOException e) {
            InfoMessages.setErrorMessage("Nepavyko įkelti");
        }
        catch (IllegalArgumentException e) {
            InfoMessages.setErrorMessage("Pasirinktas failas nėra Excel failas.");
        }
        catch (CellValueTypeException e) {
            InfoMessages.setErrorMessage("Blogo tipo langelis Excel faile.");
        }
        catch (AnswersGatheringException e) {
        } catch (ParseException e) {
            InfoMessages.setErrorMessage("Nepavyko įkelti");
        } finally {
            uploadStarted = false;
        }
   }

    private String saveSurvey(SurveyTuple tuple)
    {
        List<QuestionAnswersTuple> questionAnswersTuples = tuple.getQuestionAnswersTuples();
        Map<Integer, List<AnswerFromExcel>> answersFromExcel = tuple.getAnswers();

        Survey survey = surveyFlusher.flushSurvey(questionAnswersTuples, authenticatedUserHolder, tuple.getHeader(), isActive);
        // saving answers
        for (Object key : answersFromExcel.keySet()) {
            String errorMessage = surveyFlusher.flushAnswer(answersFromExcel.get(key), survey.getQuestionList());
            if(errorMessage != null) {
                return errorMessage;
            }
        }
        return null;
    }
}
