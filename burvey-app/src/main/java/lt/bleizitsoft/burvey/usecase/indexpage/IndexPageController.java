package lt.bleizitsoft.burvey.usecase.indexpage;


import lombok.Getter;
import lombok.Setter;
import lt.bleizitsoft.burvey.api.authCheck.AdministratorOnly;
import lt.bleizitsoft.burvey.api.authCheck.LoggedIn;
import lt.bleizitsoft.burvey.api.dao.ISurveyDAO;
import lt.bleizitsoft.burvey.api.dao.IUserProfileDAO;
import lt.bleizitsoft.burvey.api.log.LogBusinessLogicInterceptorBinding;
import lt.bleizitsoft.burvey.api.staticobjects.InfoMessages;
import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;
import lt.bleizitsoft.burvey.entities.Survey;
import lt.bleizitsoft.burvey.entities.UserProfile;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@LoggedIn
@Named
@ViewScoped
public class IndexPageController implements Serializable {

    @Inject
    private ISurveyDAO surveyDAO;

    @Inject
    private IAuthenticatedUserHolder authenticatedUserHolder;

    @Inject
    private IUserProfileDAO userProfileDAO;

    @Getter
    @Setter
    private int selectedSurveyId;

    @AdministratorOnly
    public List<Survey> getAllSurveys(){
        return surveyDAO.readAll();
    }

    public List<Survey> getPublicSurveys(){
        return surveyDAO.readByColumnIsPublic(true);
    }

    public List<Survey> getMySurveys(){
        return userProfileDAO.readById(authenticatedUserHolder.produceAuthenticatedUserProfile().getId())
                .map(UserProfile::getSurveyList).orElse(new ArrayList<>(0));
    }

    public void changeSelectedSurvey(int surveyId){
        this.selectedSurveyId = surveyId;
    }

    @LogBusinessLogicInterceptorBinding
    @Transactional
    public void deleteSurvey() {
        Survey selectedSurvey;

        if (selectedSurveyId == 0 ||
                (selectedSurvey = surveyDAO.readById(selectedSurveyId).orElse(null)) == null) {
            InfoMessages.setErrorMessage("Tokia apklausa neegzistuoja!");
            RequestContext.getCurrentInstance().execute("$('.modalPseudoClass').hide();$('.modal-backdrop').remove();");
            return;
        }


        if (!authenticatedUserHolder.isAdministrator() &&
                selectedSurvey.getOwner().getId() != authenticatedUserHolder.getAuthenticatedUserProfile().getId())
        {
            InfoMessages.setErrorMessage("Tokia apklausa neegzistuoja!");
            RequestContext.getCurrentInstance().execute("$('.modalPseudoClass').hide();$('.modal-backdrop').remove();");
            return;
        }

        surveyDAO.delete(selectedSurvey);
        InfoMessages.setInfoMessage("Apklausa pašalinta!");
        RequestContext.getCurrentInstance().execute("$('.modalPseudoClass').hide();$('.modal-backdrop').remove();");
    }

}
