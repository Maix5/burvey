package lt.bleizitsoft.burvey.usecase.Exceptions;

import javax.ejb.ApplicationException;

/**
 * Created by Povilas on 5/31/2017.
 */
@ApplicationException(rollback=true)
public class CellValueTypeException extends Exception {

    private static final long serialVersionUID = 1L;

    public CellValueTypeException() {
        super();
    }
}
