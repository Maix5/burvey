package lt.bleizitsoft.burvey.usecase.surveyreportmodule;

import lt.bleizitsoft.burvey.entities.Question;
import lt.bleizitsoft.burvey.entities.UserAnswer;

import javax.enterprise.inject.Model;
import javax.faces.bean.ManagedBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Albe on 2017-05-27.
 */
@Model
public class TextAnswersModel implements Serializable {


    public List<String> getTextMap(Question question)
    {
        List<UserAnswer> userAnswerList = question.getUserAnswerList();
        List<String> answers = new ArrayList<>();
        Integer totalCount = userAnswerList.size();
        for (UserAnswer answer : userAnswerList) {
            String content = answer.getUserAnswerText().getContentText();
            if (content.length() >= 4) {
                answers.add(content);
            }
        }
        return answers;
    }
}
