package lt.bleizitsoft.burvey.usecase.surveyreportmodule;

import lt.bleizitsoft.burvey.api.dao.IAnswerOptionDAO;
import lt.bleizitsoft.burvey.api.dao.IUserMultipleChoiceAnswerOptionDAO;
import lt.bleizitsoft.burvey.entities.AnswerOption;
import lt.bleizitsoft.burvey.entities.Question;
import lt.bleizitsoft.burvey.entities.Survey;
import lt.bleizitsoft.burvey.entities.UserMultipleChoiceAnswerOption;
import org.hibernate.Hibernate;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

import javax.enterprise.inject.Model;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Albe on 2017-05-27.
 */

@Model
public class BarChartNormalModel implements Serializable
{
    BarChartModel model;

    @Inject
    private IAnswerOptionDAO answerOptionDAO;

    @Inject
    private IUserMultipleChoiceAnswerOptionDAO userMultipleChoiceAnswerOptionDAO;

    Survey survey;

    public BarChartModel getModel(Question question){

        List<AnswerOption> answerOptionList = question.getAnswerOptionList();
        Map<Integer, Integer> resultsMap = new HashMap<>();
        Map<Integer,AnswerOption> ansOpMap = new HashMap<>();
        Integer totalAnswerCount = question.getUserAnswerList().size();

        for ( AnswerOption opt : answerOptionList) {
            Hibernate.initialize(opt.getUserMultipleChoiceAnswerOptionList());
            List<UserMultipleChoiceAnswerOption> userMultipleChoiceAnswerOptions = opt.getUserMultipleChoiceAnswerOptionList();
            resultsMap.put(opt.getId(),userMultipleChoiceAnswerOptions.size());
            ansOpMap.put(opt.getId(),opt);
        }
        model = new BarChartModel();
        model.setExtender("ext");
        ChartSeries series = new ChartSeries();
        for (Map.Entry<Integer,Integer> entry : resultsMap.entrySet())
        {
            if (totalAnswerCount==0) {
                series.set(ansOpMap.get(entry.getKey()).getContent(), 0);
            } else {
                float value = entry.getValue()*100/totalAnswerCount;
                String content = ansOpMap.get(entry.getKey()).getContent();
                if ( content.length() > 20){
                    content = content.substring(0,19)+"...";
                }
                series.set(content, value);
            }
        }

        model.addSeries(series);

        Axis xAxis = model.getAxis(AxisType.X);

        xAxis.setLabel("Klausimai");
        xAxis.setTickAngle(-60);

        Axis yAxis = model.getAxis(AxisType.Y);
        yAxis.setLabel("Atsakymų kiekis");
        yAxis.setMin(0);
        yAxis.setMax(100);
        model.setShowPointLabels(true);
        return model;
    }
}
