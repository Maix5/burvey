package lt.bleizitsoft.burvey.usecase.usercontrolmodule;

import lombok.Getter;
import lombok.Setter;
import lt.bleizitsoft.burvey.api.authCheck.NotLoggedIn;
import lt.bleizitsoft.burvey.api.dao.ICandidateInfoDAO;
import lt.bleizitsoft.burvey.api.dao.IUserProfileDAO;
import lt.bleizitsoft.burvey.api.emailService.ILinkEmailer;
import lt.bleizitsoft.burvey.api.optLock.HandleOptLock;
import lt.bleizitsoft.burvey.api.staticobjects.InfoMessages;
import lt.bleizitsoft.burvey.api.staticobjects.PagePaths;
import lt.bleizitsoft.burvey.entities.CandidateInfo;
import lt.bleizitsoft.burvey.entities.UserProfile;
import org.omnifaces.cdi.ViewScoped;

import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.File;
import java.io.Serializable;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by Albe on 2017-05-27.
 */
@NotLoggedIn
@Named
@ViewScoped
public class PasswordRecoveryRequestController implements Serializable {

    @Setter
    @Getter
    private String email;

    @Inject
    private IUserProfileDAO userProfileDAO;

    @Inject
    ICandidateInfoDAO candidateInfoDAO;

    @Inject
    private ILinkEmailer mailer;

    @Transactional
    @HandleOptLock
    public void requestPassRecovery()
    {
        Optional<CandidateInfo> infoOptional = candidateInfoDAO.readByColumnEmailAddress(email);

        if (infoOptional.isPresent())
        {
            Optional<UserProfile> userProfileOptional = userProfileDAO.readByCandidateInfo(infoOptional.get());
            if (userProfileOptional.isPresent()){
                CandidateInfo info = infoOptional.get();
                UUID randomUUID = UUID.randomUUID();
                info.setPasswordRecoveryLink(randomUUID.toString());
                try {
                    candidateInfoDAO.update(info);
                    String subject = "Slaptažodžio atkūrimas";
                    ClassLoader classLoader = getClass().getClassLoader();
                    File file = new File(classLoader.getResource("EmailTemplates/forgotPasswordTemplate.html").getFile());
                    String htmlTemplate = file.getAbsolutePath();
                    String registerLink = PagePaths.BASE_PATH + PagePaths.RECOVER_LINK+"?id=" + randomUUID.toString();
                    if (mailer.sendLinkMail(email,subject,htmlTemplate,registerLink)){
                        InfoMessages.setInfoMessage("Į paštą išsiųsta slaptažodžio atkūrimo nuoroda!");
                    } else {
                        InfoMessages.setErrorMessage("Siunčiant laišką įvyko klaida");
                    }
                } catch (Exception e)
                {
                    InfoMessages.setErrorMessage("Įvyko sistemos klaida.");
                }

            }else{
                InfoMessages.setErrorMessage("Vartotojas tokiu pašto adresu neegzistuoja");
                return;
            }
        } else {
            InfoMessages.setErrorMessage("Vartotojas tokiu pašto adresu neegzistuoja");
            return;
        }
    }

}
