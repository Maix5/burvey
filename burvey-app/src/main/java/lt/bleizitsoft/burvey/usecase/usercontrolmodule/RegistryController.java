package lt.bleizitsoft.burvey.usecase.usercontrolmodule;

import lombok.Getter;
import lombok.Setter;
import lt.bleizitsoft.burvey.api.authCheck.NotLoggedIn;
import lt.bleizitsoft.burvey.api.dao.ICandidateInfoDAO;
import lt.bleizitsoft.burvey.api.dao.IUserProfileDAO;
import lt.bleizitsoft.burvey.api.emailService.ILinkEmailer;
import lt.bleizitsoft.burvey.api.staticobjects.InfoMessages;
import lt.bleizitsoft.burvey.api.staticobjects.PagePaths;
import lt.bleizitsoft.burvey.api.utils.UrlParser;
import lt.bleizitsoft.burvey.authentification.crypto.BCrypt;
import lt.bleizitsoft.burvey.entities.CandidateInfo;
import lt.bleizitsoft.burvey.entities.UserProfile;
import lt.bleizitsoft.burvey.api.log.LogBusinessLogicInterceptorBinding;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * Created by Albe on 2017-05-25.
 */
@NotLoggedIn
@Named
@ViewScoped
public class RegistryController implements Serializable{

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String surname;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private String repeatPassword;

    @Inject
    private ILinkEmailer linkEmailer;

    @Inject
    private ICandidateInfoDAO candidateInfoDAO;

    @Inject
    private IUserProfileDAO userProfileDAO;

    @Inject
    private HttpServletRequest httpServletRequest;

    @PostConstruct
    public void initQuery() {
        String url = httpServletRequest.getRequestURL().toString();
        String servletPath = httpServletRequest.getServletPath();
        if (url.contains(PagePaths.BASE_PATH + PagePaths.REGISTER_LINK)) {
            List<String> result = UrlParser.getBaseUrl(httpServletRequest);

            if (result != null && result.size()>=2) {
                String paramName = result.get(0);
                String paramVal = result.get(1);

                if (paramName.equals("id")) {
                    Optional<CandidateInfo> cdInfo = candidateInfoDAO.readByColumnActivationLink(paramVal);

                    if (cdInfo.isPresent()) {
                        CandidateInfo info = cdInfo.get();
                        if (info.getActivationExpirationDate().compareTo(new Date())<0){
                            try {
                                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                                InfoMessages.setErrorMessage("Registracija nebegalioja, pabandykite registruotis dar kartą");
                                Faces.redirect("login");
                                return;
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        email = info.getEmailAddress();
                    } else {
                        try {
                            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                            Faces.redirect("login");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                try {
                    Faces.redirect("login");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else if (url.contains(PagePaths.BASE_PATH+PagePaths.LOGIN_LINK)){
                List<String> result = UrlParser.getBaseUrl(httpServletRequest);
                if (result != null && result.size() >= 2)
                {
                    String paramName = result.get(0);
                    String paramVal = result.get(1);
                    if (paramName.equals("register"))
                    {
                        if (paramVal.equals("success")){
                            InfoMessages.setInfoMessage("Sėkminga registracija","messages");
                        }
                        if (paramVal.equals("fail")){
                            InfoMessages.setErrorMessage("Įvyko klaida", "messages");
                        }
                        if (paramVal.equals("exists")){
                            InfoMessages.setErrorMessage(
                                    "Paskyra jau egzistuoja");
                        }
                    }

                    RequestContext rc = RequestContext.getCurrentInstance();
                    rc.update("messages");
                }
        }else{
            try {
                Faces.redirect("login");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }




    @LogBusinessLogicInterceptorBinding
    @Transactional
    public void requestRegister()
    {
        if(!isValidMail(email))
        {
            InfoMessages.setErrorMessage("Neteisingai įvestas e-pašto adresas");
        }
        Optional<CandidateInfo> candidateInfo = candidateInfoDAO.readByColumnEmailAddress(email);
        if (candidateInfo.isPresent())
        {
            Optional<UserProfile> userProfileOptional = userProfileDAO.readByCandidateInfo(candidateInfo.get());
            if (userProfileOptional.isPresent()){
                InfoMessages.setErrorMessage("Vartotojas jau egzistuoja");
                return;
            }
            CandidateInfo info =candidateInfo.get();
            UUID randomUUID = UUID.randomUUID();
            String subject = "Nuoroda į registraciją ";
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE,2);
            info.setActivationExpirationDate(calendar.getTime());
            info.setActivationLink(randomUUID.toString());
            try {
                candidateInfoDAO.update(info);

                ClassLoader classLoader = getClass().getClassLoader();
                File file = new File(classLoader.getResource("EmailTemplates/emailRegistrationTemplate.html").getFile());
                String htmlTemplate = file.getAbsolutePath();
                String registerLink = PagePaths.BASE_PATH + PagePaths.REGISTER_LINK+"?id=" + randomUUID.toString();
                if (linkEmailer.sendLinkMail(email,subject,htmlTemplate,registerLink)){
                    InfoMessages.setInfoMessage("Į paštą išsiųsta registracijos nuoroda!");
                } else {
                    InfoMessages.setErrorMessage("Siunčiant laišką įvyko klaida");
                }


            } catch (Exception e)
            {
                InfoMessages.setErrorMessage("Registracija nesėkminga.");
            }
        } else
        {
            InfoMessages.setErrorMessage("Jūsų pašto adresas nėra galimas registracijai!");
        }
    }

    @LogBusinessLogicInterceptorBinding
    private boolean isValidMail(String email)
    {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    @LogBusinessLogicInterceptorBinding
    @Transactional
    public void register()
    {
        if(password.length() < 6){
            InfoMessages.setErrorMessage("Slaptažodį turi sudaryti bent 6 simboliai!");
            return;
        }

        if(!repeatPassword.equals(password)){
            InfoMessages.setErrorMessage("Slaptažodžiai nesutampa");
            return;
        }

        UserProfile profile = new UserProfile();
        profile.setName(name);
        profile.setSurname(surname);
        profile.setHashedPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
        try {
            Optional<CandidateInfo> opInfo = candidateInfoDAO.readByColumnEmailAddress(email);
            if (opInfo.isPresent()) {
                Optional<UserProfile> userProfileOptional = userProfileDAO.readByCandidateInfo(opInfo.get());
                if (userProfileOptional.isPresent()){
                    InfoMessages.setErrorMessage("Vartotojas jau egzistuoja");
                    FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                    Faces.redirect("login");
                    return;
                }
                CandidateInfo cd = opInfo.get();
                Calendar calendar = Calendar.getInstance();
                cd.setActivationDate(calendar.getTime());
                cd.setActivationExpirationDate(null);
                cd.setActivationLink(null);
                candidateInfoDAO.update(cd);
                profile.setCandidateInfo(opInfo.get());
                profile.setAdministrator(false);
                profile.setBlocked(false);
                userProfileDAO.create(profile);
                InfoMessages.setInfoMessage("Registracija sėkminga. Dabar galite prisijungti");
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                Faces.redirect("login");
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            try {
                InfoMessages.setErrorMessage("Įvyko klaida registruojantis");
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                Faces.redirect("login");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}
