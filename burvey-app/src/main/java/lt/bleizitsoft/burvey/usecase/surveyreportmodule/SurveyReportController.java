package lt.bleizitsoft.burvey.usecase.surveyreportmodule;

import lt.bleizitsoft.burvey.api.authCheck.LoggedIn;
import lt.bleizitsoft.burvey.api.staticobjects.PagePaths;
import lt.bleizitsoft.burvey.api.utils.UrlParser;
import lt.bleizitsoft.burvey.dao.sync.QuestionDAO;
import lt.bleizitsoft.burvey.dao.sync.SurveyDAO;
import lt.bleizitsoft.burvey.dao.sync.UserAnswerNumberDAO;
import lt.bleizitsoft.burvey.dao.sync.UserAnswerTextDAO;
import lt.bleizitsoft.burvey.entities.Question;
import lt.bleizitsoft.burvey.entities.Survey;
import lt.bleizitsoft.burvey.api.log.LogBusinessLogicInterceptorBinding;
import org.omnifaces.util.Faces;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Created by Albe on 2017-05-25.
 */
@LoggedIn
@Model
public class SurveyReportController {

    private Survey survey;

    private int id;


    @Inject
    SurveyDAO surveyDAO;


    @Inject
    QuestionDAO questionDAO;

    @Inject
    UserAnswerNumberDAO userAnswerNumberDAO;

//    @Inject
//    UserAnswerOptionDAO userAnswerOptionDAO;

    @Inject
    UserAnswerTextDAO userAnswerTextDAO;

    @Inject
    private HttpServletRequest httpServletRequest;


    @PostConstruct
    public void initQuery() {
        String url = httpServletRequest.getRequestURL().toString();
        if (url.contains(PagePaths.BASE_PATH + PagePaths.REPORT_LINK)) {
            List<String> result = UrlParser.getBaseUrl(httpServletRequest);

            if (result != null && result.size()>=2) {
                String paramName = result.get(0);
                String paramVal = result.get(1);

                if (paramName.equals("access_link")) {
                    Optional<Survey> surveyOptional = surveyDAO.readByColumnAccessLink(paramVal);

                    if (surveyOptional.isPresent()) {
                        survey = surveyOptional.get();
                        id = survey.getId();

                    } else {
                        try {
                            FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                            Faces.redirect("index");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                try {
                    FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                    Faces.redirect("index");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @LogBusinessLogicInterceptorBinding
    public List<Question> getSurveyQuestions()
    {
        return questionDAO.readByColumnSurveyId(id);
    }

    @LogBusinessLogicInterceptorBinding
    public Integer getQuestionAnswersCount(Question question){
        return question.getUserAnswerList().size();
    }
}
