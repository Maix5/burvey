package lt.bleizitsoft.burvey.usecase.surveyreportmodule;

import lt.bleizitsoft.burvey.api.dao.INumberQuestionDAO;
import lt.bleizitsoft.burvey.entities.Question;
import lt.bleizitsoft.burvey.entities.UserAnswerNumber;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;

import javax.enterprise.inject.Model;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by Albe on 2017-05-27.
 */
@Model
public class BarChartDistrModel implements Serializable {

    private BarChartModel model;

    @Inject
    private INumberQuestionDAO numberQuestionDAO;

    public BarChartModel getModel(Question question) {

        //List<UserAnswerNumber> userAnswerNumbers = numberQuestionDAO.readUserAnswerNumberByNumberQuestionId(question.getId());
        List<UserAnswerNumber> userAnswerNumbers = numberQuestionDAO.readUserAnswerNumberByNumberQuestionId(question.getNumberQuestion().getId());
        List<Integer> answerValues = new ArrayList<>();
        int totalCount = 0;
        int sum = 0;
        for (UserAnswerNumber numb : userAnswerNumbers ) {
            answerValues.add(numb.getContentNumber());
            totalCount++;
            sum = sum + numb.getContentNumber();
        }
        Collections.sort(answerValues);

        float avg;
        double median ;
        if (totalCount!=0){
            avg = sum/totalCount;
            if (answerValues.size()%2 != 0 && answerValues.size() != 1)
            {
                median = ((double)answerValues.get(answerValues.size()/2) + answerValues.get(answerValues.size()/2-1))/2;
            } else if (answerValues.size() == 1){
                median = answerValues.get(0);
            } else {
                median = (double) answerValues.get(answerValues.size()/2);
            }
        }else {
            avg = 0;
            median = 0;
        }

        model = new BarChartModel();
        model.setExtender("ext");

        ChartSeries series = new ChartSeries();
        series.set("Vidurkis", avg);
        series.set("Mediana", median);
        if (answerValues.size()==1){
            series.set("Moda",answerValues.get(0));
        } else if (answerValues.size() == 0){
            series.set("Moda",0);
        }else {
            int modeCount = 1;
            for (Integer mode : getModes(answerValues)) {
                series.set("Moda "+ modeCount ,mode);
                modeCount++;
            }
        }

        Axis xAxis = model.getAxis(AxisType.X);

        xAxis.setTickAngle(-60);

        model.addSeries(series);
        model.setShowPointLabels(true);
        return model;
    }

    private List<Integer> getModes(final List<Integer> numbers) {
        final Map<Integer, Long> countFrequencies = numbers.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        final long maxFrequency = countFrequencies.values().stream()
                .mapToLong(count -> count)
                .max().orElse(-1);

        return countFrequencies.entrySet().stream()
                .filter(tuple -> tuple.getValue() == maxFrequency)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}
