package lt.bleizitsoft.burvey.usecase.answersgatheringmodule;

import lombok.Getter;
import lombok.Setter;
import lt.bleizitsoft.burvey.api.async.RescueOrAsync;
import lt.bleizitsoft.burvey.api.dao.*;
import lt.bleizitsoft.burvey.api.emailService.ILinkEmailer;
import lt.bleizitsoft.burvey.api.staticobjects.InfoMessages;
import lt.bleizitsoft.burvey.api.staticobjects.PagePaths;
import lt.bleizitsoft.burvey.api.staticobjects.SurveyQuestionType;
import lt.bleizitsoft.burvey.api.utils.UrlParser;
import lt.bleizitsoft.burvey.entities.*;
import lt.bleizitsoft.burvey.api.log.LogBusinessLogicInterceptorBinding;
import lt.bleizitsoft.burvey.usecase.Exceptions.AnswersGatheringException;
import org.apache.deltaspike.core.api.future.Futureable;
import org.omnifaces.util.Faces;
import org.primefaces.context.RequestContext;

import javax.annotation.PostConstruct;
import javax.ejb.AsyncResult;
import javax.faces.context.FacesContext;
import javax.faces.context.PartialViewContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.SystemException;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import static javax.transaction.Transactional.TxType.REQUIRES_NEW;

/**
 * Created by Monika.Kelpsaite on 5/7/2017.
 */
@ViewScoped
@Named
public class AnswersGatheringController implements Serializable {

    private enum CURRENT_ACTION {
        ANSWER, DONE
    }

    private Future<Map<String,String>> resultInFuture = null;

    // DAO:
    @Inject private IQuestionDAO questionDAO;
    @Inject private ISurveyDAO surveyDAO;
    @Inject private IUserAnswerDAO userAnswerDAO;
    @Inject private IUserAnswerTextDAO userAnswerTextDAO;
    @Inject private IUserSingleAnswerOptionDAO userSingleAnswerOptionDAO;
    @Inject private IUserMultipleChoiceAnswerOptionDAO
            userMultipleChoiceAnswerOptionDAO;
    @Inject private IUserAnswerNumberDAO userAnswerNumberDAO;
    @Inject private IAnswerOptionDAO answerOptionDAO;
    @Inject private IAnswerAccessRegistryDAO answerAccessRegistryDAO;

    // Async DAO:
    @Inject @RescueOrAsync
    private IUserAnswerDAO asyncUserAnswerDAO;
    @Inject @RescueOrAsync
    private IAnswerAccessRegistryDAO asyncAnswerAccessRegistryDAO;
    @Inject @RescueOrAsync
    private IQuestionDAO asyncQuestionDAO;
    @Inject @RescueOrAsync
    private IAnswerOptionDAO asyncAnswerOptionDAO;
    @Inject @RescueOrAsync
    private IUserSurveyAnswerGroupDAO asyncUserSurveyAnswerGroupDAO;

    // Variables:
    @Inject private ILinkEmailer sender;
    @Inject HttpServletRequest request;
    @Getter
    private Survey survey;
    @Getter @Setter
    private String userEmail;
    @Getter
    private Map<Integer, Object> userAnswers = new HashMap<Integer, Object>();
    private Map<Integer, Integer> existingUserAnswers = new HashMap<>();
    private CURRENT_ACTION currentAction = CURRENT_ACTION.ANSWER;
    private String emailHtmlPattern;
    private String accessLink = null;
    private String answersRegistry;


    @PostConstruct
    public void init() {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("EmailTemplates/emailSurveyLinkTemplate.html").getFile());
        emailHtmlPattern = file.getAbsolutePath();
        if (isAjaxRequest()) return;

        List<String> queryParams = UrlParser.getBaseUrl(request);
        if (queryParams.size() < 2) {
            setErrorAndRedirect(null, "index");
            return;
        }
        String surveyAccessLink = queryParams.get(1);
        AnswersAccessRegistry answersAccessRegistry = null;
        List<UserAnswer> userAnswerList = null;

        if (queryParams.get(0).equals("answers_access_link")) {
            answersAccessRegistry =
                    answerAccessRegistryDAO.readByColumnAccessLink(surveyAccessLink).orElse(null);
            if (answersAccessRegistry == null) {
                setErrorAndRedirect("Tokia apklausa neegzistuoja!", "index");
                return;
            }

            userAnswerList = answersAccessRegistry.getUserSurveyAnswerGroup().getUserAnswerList();

            if (userAnswerList != null && userAnswerList.size() > 0) {
                surveyAccessLink = userAnswerList.get(0).getQuestion().getSurvey().getAccessLink();
            }
        }

        if (getSurvey(surveyAccessLink) == null) {
            setErrorAndRedirect("Tokia apklausa neegzistuoja!", "index");
            return;
        }

        if (new Date().after(survey.getExpirationDate()) || !survey.getIsActive()) {
            currentAction = currentAction.DONE;
            InfoMessages.setErrorMessage("Aplausa neaktyvi!");
            return;
        }

        //saved survey
        if (queryParams.get(0).equals("answers_access_link")) {
            accessLink = queryParams.get(1);
            for (UserAnswer userAnswer : userAnswerList) {
                answersRegistry = queryParams.get(1);
                existingUserAnswers.put(userAnswer.getQuestion().getId(), userAnswer.getId());
                switch (userAnswer.getQuestion().getQuestionType()) {
                    case SurveyQuestionType.text:
                        UserAnswerText userAnswerText = userAnswerTextDAO.readById(userAnswer.getUserAnswerText().getId()).orElse(null);
                        userAnswers.put(userAnswer.getQuestion().getId(), userAnswerText.getContentText());
                        break;
                    case SurveyQuestionType.checkbox:
                        UserMultipleChoiceAnswerOption userAnswerOptionMultiple =
                                userMultipleChoiceAnswerOptionDAO.readById(userAnswer.getUserMultipleChoiceAnswerOption().getId()).orElse(null);
                        List<AnswerOption> answersList = userAnswerOptionMultiple.getAnswerOptionList();
                        userAnswers.put(
                                userAnswer.getQuestion().getId(),
                                answersList.stream().map(
                                        answerOption -> answerOption.getId())
                                .collect(Collectors.toList())
                                .toArray());
                        break;
                    case SurveyQuestionType.multipleChoice:
                        UserSingleAnswerOption userAnswerOption =
                                userSingleAnswerOptionDAO.readById(userAnswer.getUserSingleAnswerOption().getId()).orElse(null);
                        userAnswers.put(userAnswer.getQuestion().getId(), userAnswerOption.getAnswerOption().getId());
                        break;
                    case SurveyQuestionType.scale:
                        UserAnswerNumber userAnswerNumber =
                                userAnswerNumberDAO.readById(userAnswer.getUserAnswerNumber().getId()).orElse(null);
                        userAnswers.put(userAnswer.getQuestion().getId(), userAnswerNumber.getContentNumber());
                        break;
                }
            }
        }
    }

    public boolean isCurrentAction(CURRENT_ACTION action) {
        return currentAction == action;
    }

    // take survey from DB and fill in with them html
    public Survey getSurvey(String surveyLink) {
        survey = surveyDAO.readByColumnAccessLink(surveyLink).orElse(null);
        survey.getQuestionList().sort((x, y) -> x.getOrderNr().compareTo(y.getOrderNr()));
        return survey;
    }

    // when user push button "submit", all answers saving to DB.
    @LogBusinessLogicInterceptorBinding
    @Transactional
    public void submitAnswers() throws ExecutionException, InterruptedException {
        if(userEmail == "") {
            userEmail = null;
        }
        Map<String,String> result = saveAllAnswers();
        while(result.containsKey("WAIT")) {
            //Thread.sleep(10000);
            result = saveAllAnswers();
        }
        if(result.containsKey("ERROR")) {
            InfoMessages.setErrorMessage(result.get("ERROR"), "globalMessages");
        }
        else {
            resultInFuture = null;
            InfoMessages.setInfoMessage(result.get("OK"), "globalMessages");
            currentAction = CURRENT_ACTION.DONE;
            RequestContext rc = RequestContext.getCurrentInstance();
            rc.update("surveyContent");
        }
    }

    @LogBusinessLogicInterceptorBinding
    @Transactional
    public void submitEmail() throws ExecutionException, InterruptedException {
        //check if email is correct and then save answers
        if (userEmail == null || userEmail.isEmpty() || !sender.isValidEmailAddress(userEmail)) {
            InfoMessages.setErrorMessage("Nenurodytas joks el. paštas arba nurodytas paštas neegzistuoja", "modalMessages");
            return;
        }
        submitAnswers();
    }

    public Map<String,String> saveAllAnswers() throws ExecutionException, InterruptedException {
        Map<String,String> response = new HashMap<>();
        if (resultInFuture == null) {
            try {
                resultInFuture = saveAnswers();
            } catch (SystemException e) {
                response.put("ERROR", "Sistemos klaida");
                return response;
            }
            if(userEmail != null) {
                response.put("WAIT", "");
            }
            else {
                response.put("WAIT", "");
            }
            return response;
        } else {
            if (resultInFuture.isDone()) {
                response = resultInFuture.get();
                return response;
            } else {
                response.put("WAIT", "");
                return response;
            }
        }
    }

    // when user push button "save partially filled survey" . It is different way how to say
    // answers when we are saving in "submitAnswers" method. In this method body we are asking for email address of this user
    @Futureable
    @Transactional(value = REQUIRES_NEW)
    private Future<Map<String,String>> saveAnswers() throws SystemException {
        Map<String,String> response = new HashMap<>();
        try {
            sendAndSave();
            if(userEmail != null) {
                response.put("OK", "Atsakymai išsaugoti ir nuoroda nusiųsta į el. paštą");
            }
            else {
                response.put("OK", "Atsakymai išsaugoti");
            }
            return new AsyncResult<>(response);
        }
        catch(AnswersGatheringException ex) {
            response.put("ERROR", "Atsakymų išsaugoti nepavyko");
            return new AsyncResult<>(response);
        }
    }

    private void sendEmail(String surveyLink) throws AnswersGatheringException {
        String subject = "Nuoroda į nepabaigtą pildyti apklausą - " + survey.getTitle();
        if(!sender.sendLinkMail(userEmail, subject, emailHtmlPattern, surveyLink))
            throw new AnswersGatheringException();
    }

    private void sendAndSave() throws AnswersGatheringException {
        AnswersAccessRegistry accessRegistry = null;
        UserSurveyAnswerGroup answerGroup = new UserSurveyAnswerGroup();

        if (accessLink != null) {
            deleteAllAnswers();
        }

        if (userEmail != null) {
            accessRegistry = new AnswersAccessRegistry();
            accessRegistry.setAccessLink(UUID.randomUUID().toString());
            answerGroup.setAnswersAccessRegistry(accessRegistry);
            asyncAnswerAccessRegistryDAO.create(accessRegistry);
        }

        for (Map.Entry<Integer, Object> answer : userAnswers.entrySet()) {
            if (answer.getValue() == null
                    || answer.getValue().toString().equals("")) {
                continue;
            }
            Optional<Question> maybeQuestion =
                    asyncQuestionDAO.readById(answer.getKey());
            Question question;
            if (!maybeQuestion.isPresent()) continue;
            else question = maybeQuestion.get();

            //this answer may already exist, maybe we need update it?
            UserAnswer userAnswer = new UserAnswer();
            userAnswer.setQuestion(question);
            userAnswer.setUserSurveyAnswerGroup(answerGroup);

            switch (question.getQuestionType()) {
                case SurveyQuestionType.text:
                    UserAnswerText userAnswerText = new UserAnswerText();
                    userAnswerText.setContentText((String) answer.getValue());
                    userAnswerText.setUserAnswer(userAnswer);
                    userAnswer.setUserAnswerText(userAnswerText);
                    break;
                case SurveyQuestionType.checkbox:
                    Object[] myArray = (Object[]) answer.getValue();
                    List<AnswerOption> answerOptions = new ArrayList<>();

                    for (Object currentObject : myArray) {
                        try {
                            int selectedOption = Integer.parseInt(currentObject.toString());
                            AnswerOption answerOption =
                                    asyncAnswerOptionDAO
                                            .readById(selectedOption)
                                            .orElse(null);
                            if (answerOption == null) continue;
                            answerOptions.add(answerOption);
                        } catch (ClassCastException ex) {
                            throw ex;
                        }
                    }

                    UserMultipleChoiceAnswerOption userAnswerOptions = new UserMultipleChoiceAnswerOption();
                    userAnswerOptions.setAnswerOptionList(answerOptions);
                    answerOptions.forEach(x -> x.getUserMultipleChoiceAnswerOptionList().add(userAnswerOptions));
                    userAnswerOptions.setUserAnswer(userAnswer);
                    userAnswer.setUserMultipleChoiceAnswerOption(userAnswerOptions);
                    break;
                case SurveyQuestionType.multipleChoice:
                    int selectedOption = Integer.parseInt(answer.getValue().toString());
                    AnswerOption answerOption =
                            asyncAnswerOptionDAO
                                    .readById(selectedOption).orElse(null);

                    UserSingleAnswerOption userAnswerSingleOption = new UserSingleAnswerOption();
                    if (answerOption == null) continue;
                    userAnswerSingleOption.setAnswerOption(answerOption);
                    userAnswerSingleOption.setUserAnswer(userAnswer);
                    userAnswer.setUserSingleAnswerOption(userAnswerSingleOption);
                    break;
                case SurveyQuestionType.scale:
                    UserAnswerNumber userAnswerNumber = new UserAnswerNumber();
                    userAnswerNumber.setContentNumber(Integer.parseInt(answer.getValue().toString()));
                    userAnswerNumber.setUserAnswer(userAnswer);
                    userAnswer.setUserAnswerNumber(userAnswerNumber);

                    break;
            }

            asyncUserAnswerDAO.create(userAnswer);
        }
        if (userEmail != null) {
            sendEmail(PagePaths.BASE_PATH + PagePaths.ANSWER_SURVEY_LINK + "?answers_access_link=" + accessRegistry.getAccessLink());
        }
    }

    private void deleteAllAnswers() {
        if (accessLink == null) return;

        AnswersAccessRegistry accessRegistry = answerAccessRegistryDAO.readByColumnAccessLink(accessLink).orElse(null);
        if (accessRegistry == null) return;

        for (UserAnswer answer :  accessRegistry.getUserSurveyAnswerGroup().getUserAnswerList()) {
            UserAnswer currentAnswer = userAnswerDAO.readById(answer.getId()).orElse(null);
            if (currentAnswer == null) continue;
            userAnswerDAO.delete(currentAnswer);
        }

        answerAccessRegistryDAO.delete(accessRegistry);
    }

    private boolean isAjaxRequest() {
        PartialViewContext partialViewContext = FacesContext.getCurrentInstance().getPartialViewContext();
        return null != partialViewContext && partialViewContext.isAjaxRequest();
    }

    private void setErrorAndRedirect(String message, String redirectPage) {
        if (message != null) {
            InfoMessages.setErrorMessage(message);
        }

        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        try {
            Faces.redirect(redirectPage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}