package lt.bleizitsoft.burvey.usecase.surveyimportermodule;

import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;
import lt.bleizitsoft.burvey.api.dao.*;
import lt.bleizitsoft.burvey.api.staticobjects.SurveyQuestionType;
import lt.bleizitsoft.burvey.entities.*;
import lt.bleizitsoft.burvey.parser.AnswerFromExcel;
import lt.bleizitsoft.burvey.parser.QuestionAnswersTuple;
import lt.bleizitsoft.burvey.parser.SurveyHeader;

import javax.enterprise.inject.Model;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.*;

/**
 * Created by Povilas on 5/31/2017.
 */
@Model
public class SurveyFlusher implements ISurveyFlusher {

    @Inject
    private ISurveyDAO surveyDAO;
    @Inject
    private IUserSurveyAnswerGroupDAO userSurveyAnswerGroupDAO;
    @Inject
    private IUserAnswerTextDAO userAnswerTextDAO;
    @Inject
    private IUserSingleAnswerOptionDAO userSingleAnswerOptionDAO;
    @Inject
    private IUserAnswerNumberDAO userAnswerNumberDAO;
    @Inject
    private IUserMultipleChoiceAnswerOptionDAO userMultipleChoiceAnswerOptionDAO;
    @Inject
    private IUserAnswerDAO userAnswerDAO;

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public Survey flushSurvey(List<QuestionAnswersTuple> questionAnswersTuples, IAuthenticatedUserHolder userHolder,
                              SurveyHeader header, boolean isActive) {
        // saving survey
        Survey survey = new Survey();
        survey.setOwner(userHolder.getAuthenticatedUserProfile());
        survey.setTitle(header.getName());
        survey.setActivationDate(new Date());
        survey.setExpirationDate(header.getValidate());
        survey.setIsActive(isActive);
        survey.setIsPublic(header.isPublic());
        survey.setAccessLink(UUID.randomUUID().toString());
        survey.setQuestionsPerPage(5);
        survey.setDescription(header.getDescription());
        survey.setQuestionList(new ArrayList());
        // saving questions
        List<Question> questions = new ArrayList<Question>();
        for (int i = 0; i < questionAnswersTuples.size(); i++) {
            QuestionAnswersTuple questionAnswersTuple = questionAnswersTuples.get(i);
            Question question = new Question();
            question.setSurvey(survey);
            question.setRequired(questionAnswersTuple.getQuestion().isMandatory());
            question.setQuestionType(questionAnswersTuple.getQuestion().getQuestionType());
            question.setOrderNr(questionAnswersTuple.getQuestion().getQuestionNumber());
            question.setContent(questionAnswersTuple.getQuestion().getQuestion());
            if (SurveyQuestionType.scale.equalsIgnoreCase(questionAnswersTuple.getQuestion().getQuestionType())) {
                NumberQuestion numberQuestion = new NumberQuestion();
                int firstNumber = Integer.parseInt(questionAnswersTuple.getOptionList().get(0));
                int secondNumber = Integer.parseInt(questionAnswersTuple.getOptionList().get(1));
                if (firstNumber > secondNumber) {
                    numberQuestion.setMaximumRange(firstNumber);
                    numberQuestion.setMinimalRange(secondNumber);
                } else {
                    numberQuestion.setMaximumRange(secondNumber);
                    numberQuestion.setMinimalRange(firstNumber);
                }
                numberQuestion.setQuestion(question);
                question.setNumberQuestion(numberQuestion);
            } else {
                question.setAnswerOptionList(new ArrayList<AnswerOption>());
                for (int j = 0; j < questionAnswersTuple.getOptionList().size(); j++) {
                    AnswerOption option = new AnswerOption();
                    option.setContent(questionAnswersTuple.getOptionList().get(j));
                    option.setQuestion(question);
                    question.getAnswerOptionList().add(option);
                }
            }
            survey.getQuestionList().add(question);
            questions.add(question);
        }
        surveyDAO.create(survey);
        return survey;
    }

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public String flushAnswer(List<AnswerFromExcel> answers, List<Question> questions) {
        UserSurveyAnswerGroup userSurveyAnswerGroup = new UserSurveyAnswerGroup();
        userSurveyAnswerGroupDAO.create(userSurveyAnswerGroup);
        Map<Question, List<AnswerOption>> checkboxAnswers = new HashMap<Question, List<AnswerOption>>();

        for(int i = 0; i < answers.size(); i++) {
            AnswerFromExcel currentAnswer = answers.get(i);
            Optional<Question> currentQuestion = questions.stream().
                    filter(q -> (q.getOrderNr() == currentAnswer.getQuestionNumber())).
                    findFirst();
            if (!currentQuestion.isPresent()) {
                return "Nėra klausimo į kurį pateiktas atsakymas.";
            }
            Question currentQuestionInstance = currentQuestion.get();

            if(SurveyQuestionType.text.equalsIgnoreCase(currentQuestionInstance.getQuestionType())) {
                UserAnswerText userAnswerText = new UserAnswerText();
                userAnswerText.setUserAnswer(createUserAnswer(currentQuestionInstance, userSurveyAnswerGroup));
                userAnswerText.setContentText(currentAnswer.getAnswer());
                userAnswerTextDAO.create(userAnswerText);
            } else if (SurveyQuestionType.checkbox.equalsIgnoreCase(currentQuestionInstance.getQuestionType())) {
                try {
                    int answerNumber = Integer.parseInt(currentAnswer.getAnswer());

                    if (!checkboxAnswers.containsKey(currentQuestionInstance)) {
                        checkboxAnswers.put(currentQuestionInstance, new ArrayList<AnswerOption>());
                    }
                    List<AnswerOption> answerOptionList = currentQuestionInstance.getAnswerOptionList();
//                        Collections.sort(answerOptionList, (o1, o2) -> o1.getId().compareTo(o2.getId()));
                    if(answerOptionList.size() < answerNumber || answerNumber < 0) {
                        return SurveyQuestionType.checkbox + " tipo klausimui pateiktas atsakymo varianto numeris neegzistuoja.";
                    }

                    AnswerOption answerOption = answerOptionList.get(answerNumber - 1);
                    if (answerOption.getUserMultipleChoiceAnswerOptionList() == null) {
                        answerOption.setUserMultipleChoiceAnswerOptionList(new ArrayList<UserMultipleChoiceAnswerOption>());
                    }
                    checkboxAnswers.get(currentQuestionInstance).add(answerOption);
                }
                catch (NumberFormatException ex) {
                    return SurveyQuestionType.checkbox + " tipo klausimui pateiktas atsakymas nėra skaičius (turi būti nurodytas atsakymo varianto numeris)";
                }
            } else if (SurveyQuestionType.multipleChoice.equalsIgnoreCase(currentQuestionInstance.getQuestionType())) {
                UserSingleAnswerOption userSingleAnswerOption = new UserSingleAnswerOption();
                userSingleAnswerOption.setUserAnswer(createUserAnswer(currentQuestionInstance, userSurveyAnswerGroup));
                List<AnswerOption> answerOptionList = currentQuestionInstance.getAnswerOptionList();
                try {
                    int answerNumber = Integer.parseInt(currentAnswer.getAnswer());
//                        Collections.sort(answerOptionList, (o1, o2) -> o1.getId().compareTo(o2.getId()));
                    if(answerOptionList.size() < answerNumber || answerNumber < 0) {
                        return SurveyQuestionType.multipleChoice + " tipo klausimui pateiktas atsakymo varianto numeris neegzistuoja.";
                    }
                    userSingleAnswerOption.setAnswerOption(answerOptionList.get(answerNumber - 1));
                    userSingleAnswerOptionDAO.create(userSingleAnswerOption);
                }
                catch (NumberFormatException ex) {
                    return SurveyQuestionType.multipleChoice + " tipo klausimui pateiktas atsakymas nėra skaičius (turi būti nurodytas atsakymo varianto numeris)";
                }
            } else if (SurveyQuestionType.scale.equalsIgnoreCase(currentQuestionInstance.getQuestionType())) {
                NumberQuestion currentNumberQuestion = currentQuestionInstance.getNumberQuestion();
                if(currentNumberQuestion.getMaximumRange() < Integer.parseInt(currentAnswer.getAnswer()) || currentNumberQuestion.getMinimalRange() > Integer.parseInt(currentAnswer.getAnswer())) {
                    return SurveyQuestionType.scale + " tipo klausimo atsakymas netinkamas.";
                }
                UserAnswerNumber userAnswerNumber = new UserAnswerNumber();
                userAnswerNumber.setUserAnswer(createUserAnswer(currentQuestionInstance, userSurveyAnswerGroup));
                userAnswerNumber.setContentNumber(new Integer(Integer.parseInt(currentAnswer.getAnswer())));
                userAnswerNumberDAO.create(userAnswerNumber);
            }
        }
        for (Object question : checkboxAnswers.keySet()) {
            UserMultipleChoiceAnswerOption userMultipleChoiceAnswerOption = new UserMultipleChoiceAnswerOption();
            userMultipleChoiceAnswerOption.setUserAnswer(createUserAnswer((Question)question, userSurveyAnswerGroup));
            List<AnswerOption> answerOptionList = checkboxAnswers.get(question);
            userMultipleChoiceAnswerOption.setAnswerOptionList(answerOptionList);
            answerOptionList.forEach(x -> x.getUserMultipleChoiceAnswerOptionList().add(userMultipleChoiceAnswerOption));
            userMultipleChoiceAnswerOptionDAO.create(userMultipleChoiceAnswerOption);
        }
        return null;
    }

    private UserAnswer createUserAnswer(Question currentQuestionInstance, UserSurveyAnswerGroup userSurveyAnswerGroup) {
        UserAnswer userAnswer = new UserAnswer();
        userAnswer.setQuestion(currentQuestionInstance);
        userAnswer.setUserSurveyAnswerGroup(userSurveyAnswerGroup);
        userAnswerDAO.create(userAnswer);
        return userAnswer;
    }
}
