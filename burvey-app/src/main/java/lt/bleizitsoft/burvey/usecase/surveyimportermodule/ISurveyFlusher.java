package lt.bleizitsoft.burvey.usecase.surveyimportermodule;

import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;
import lt.bleizitsoft.burvey.entities.Question;
import lt.bleizitsoft.burvey.entities.Survey;
import lt.bleizitsoft.burvey.parser.AnswerFromExcel;
import lt.bleizitsoft.burvey.parser.QuestionAnswersTuple;
import lt.bleizitsoft.burvey.parser.SurveyHeader;

import java.util.List;

/**
 * Created by Povilas on 5/31/2017.
 */
public interface ISurveyFlusher {

    Survey flushSurvey(List<QuestionAnswersTuple> questionAnswersTuples, IAuthenticatedUserHolder userHolder, SurveyHeader header, boolean isActive);

    String flushAnswer(List<AnswerFromExcel> answers, List<Question> questions);
}
