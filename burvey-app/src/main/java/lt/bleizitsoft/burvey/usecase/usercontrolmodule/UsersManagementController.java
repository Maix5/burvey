package lt.bleizitsoft.burvey.usecase.usercontrolmodule;

import lombok.Getter;
import lombok.Setter;
import lt.bleizitsoft.burvey.EmailService.Emailer;
import lt.bleizitsoft.burvey.api.authCheck.AdministratorOnly;
import lt.bleizitsoft.burvey.api.dao.ICandidateInfoDAO;
import lt.bleizitsoft.burvey.api.dao.IUserProfileDAO;
import lt.bleizitsoft.burvey.api.emailService.ILinkEmailer;
import lt.bleizitsoft.burvey.api.staticobjects.InfoMessages;
import lt.bleizitsoft.burvey.entities.CandidateInfo;
import lt.bleizitsoft.burvey.entities.UserProfile;
import lt.bleizitsoft.burvey.api.log.LogBusinessLogicInterceptorBinding;

import javax.enterprise.inject.Model;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;

@Model
@AdministratorOnly
public class UsersManagementController {

    @Inject
    private IUserProfileDAO userProfileDAO;

    @Inject
    private ICandidateInfoDAO candidateInfoDAO;

    @Inject
    private ILinkEmailer emailer;

    @Getter @Setter
    private String email;

    public List<CandidateInfo> getAllUsers() { return candidateInfoDAO.readAll(); }

    @LogBusinessLogicInterceptorBinding
    @Transactional
    public void blockUser (int userId){
        UserProfile user = userProfileDAO.readById(userId).orElse(null);
        if (user == null) {
            return;
        }
        user.setBlocked(true);
        userProfileDAO.update(user);
    }

    @LogBusinessLogicInterceptorBinding
    @Transactional
    public void unblockUser (int userId){
        UserProfile user = userProfileDAO.readById(userId).orElse(null);
        if (user == null) {
            return;
        }
        user.setBlocked(false);
        userProfileDAO.update(user);
    }

    @LogBusinessLogicInterceptorBinding
    @Transactional
    public void makeAdmin (int userId){
        UserProfile user = userProfileDAO.readById(userId).orElse(null);
        if (user == null) {
            return;
        }
        user.setAdministrator(true);
        userProfileDAO.update(user);
    }

    @LogBusinessLogicInterceptorBinding
    @Transactional
    public void removeAdmin (int userId){
        UserProfile user = userProfileDAO.readById(userId).orElse(null);
        if (user == null) {
            return;
        }
        user.setAdministrator(false);
        userProfileDAO.update(user);
    }

    @LogBusinessLogicInterceptorBinding
    @Transactional
    public void removeCandidate(int userId) {
        candidateInfoDAO.delete(candidateInfoDAO.readById(userId).get());
        InfoMessages.setInfoMessage("Naudotojas pašalintas!");
    }

    @LogBusinessLogicInterceptorBinding
    @Transactional
    public void addNewUser() {

        if (!emailer.isValidEmailAddress(email)) {
            InfoMessages.setErrorMessage("Įvestas klaidingas el. pašto adresas!");
            return;
        }

        if (candidateInfoDAO.readByColumnEmailAddress(email).isPresent()) {
            InfoMessages.setErrorMessage("Naudotojas su tokiu el. paštu jau egzistuoja!");
            return;
        }

        CandidateInfo candidateInfo = new CandidateInfo();
        candidateInfo.setEmailAddress(email);
        candidateInfoDAO.create(candidateInfo);
        InfoMessages.setInfoMessage("Naudotojas pridėtas!");
    }

}
