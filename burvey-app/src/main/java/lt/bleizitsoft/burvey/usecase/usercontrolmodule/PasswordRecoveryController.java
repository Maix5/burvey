package lt.bleizitsoft.burvey.usecase.usercontrolmodule;

import lombok.Getter;
import lombok.Setter;
import lt.bleizitsoft.burvey.api.authCheck.NotLoggedIn;
import lt.bleizitsoft.burvey.api.dao.ICandidateInfoDAO;
import lt.bleizitsoft.burvey.api.dao.IUserProfileDAO;
import lt.bleizitsoft.burvey.api.optLock.HandleOptLock;
import lt.bleizitsoft.burvey.api.staticobjects.InfoMessages;
import lt.bleizitsoft.burvey.api.staticobjects.PagePaths;
import lt.bleizitsoft.burvey.api.utils.UrlParser;
import lt.bleizitsoft.burvey.authentification.crypto.BCrypt;
import lt.bleizitsoft.burvey.entities.CandidateInfo;
import lt.bleizitsoft.burvey.entities.UserProfile;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * Created by Albe on 2017-05-27.
 */
@NotLoggedIn
@Named
@ViewScoped
public class PasswordRecoveryController implements Serializable {

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String password;

    @Inject
    private ICandidateInfoDAO candidateInfoDAO;

    @Inject
    private IUserProfileDAO userProfileDAO;

    @Inject
    private HttpServletRequest httpServletRequest;

    @PostConstruct
    public void initQuery() {
        String url = httpServletRequest.getRequestURL().toString();
        if (url.contains(PagePaths.BASE_PATH + PagePaths.RECOVER_LINK)) {
            List<String> result = UrlParser.getBaseUrl(httpServletRequest);

            if (result != null && result.size()>=2) {
                String paramName = result.get(0);
                String paramVal = result.get(1);

                if (paramName.equals("id")) {
                    Optional<CandidateInfo> cdInfo = candidateInfoDAO.readByColumnPasswordRecoveryLink(paramVal);

                    if (cdInfo.isPresent()) {
                        CandidateInfo info = cdInfo.get();
                        email = info.getEmailAddress();
                    } else {
                        try {
                            Faces.redirect("login");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                try {
                    Faces.redirect("login");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else{
            try {
                Faces.redirect("login");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Transactional
    @HandleOptLock
    public void changePassword(){
        if(password.length() < 6){
            InfoMessages.setErrorMessage("Slaptažodį turi sudaryti bent 6 simboliai!");
            return;
        }

        Optional<CandidateInfo> candidateInfoOptional = candidateInfoDAO.readByColumnEmailAddress(email);
        if (candidateInfoOptional.isPresent()) {
            CandidateInfo info = candidateInfoOptional.get();
            Optional<UserProfile> userProfileOptional = userProfileDAO.readByCandidateInfo(info);
            if (userProfileOptional.isPresent()){
                try{
                UserProfile userProfile = userProfileOptional.get();
                info.setPasswordRecoveryLink(null);
                candidateInfoDAO.update(info);
                userProfile.setHashedPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
                userProfileDAO.update(userProfile);
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                InfoMessages.setInfoMessage("Slaptažodis pakeistas");
                Faces.redirect("login");
                }catch (Exception e){
                    InfoMessages.setErrorMessage("Įvyko klaida keičiant slaptažodį");
                }
            } else {

                try {
                    FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                    InfoMessages.setErrorMessage("Tokio vartotojo nėra");
                    Faces.redirect("login");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else {
            try {
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
                InfoMessages.setErrorMessage("Tokio vartotojo nėra");
                Faces.redirect("login");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
