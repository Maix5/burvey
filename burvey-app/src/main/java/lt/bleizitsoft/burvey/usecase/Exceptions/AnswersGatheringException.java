package lt.bleizitsoft.burvey.usecase.Exceptions;

import javax.ejb.ApplicationException;

/**
 * Created by Monika.Kelpsaite on 5/23/2017.
 */
@ApplicationException(rollback=true)
public class AnswersGatheringException extends Exception {

    private static final long serialVersionUID = 1L;

    public AnswersGatheringException(){
        super();
    }

}
