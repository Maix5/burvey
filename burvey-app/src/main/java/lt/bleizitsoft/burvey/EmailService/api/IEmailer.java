package lt.bleizitsoft.burvey.EmailService.api;

/**
 * Created by marius on 5/31/17.
 */
public interface IEmailer {

    boolean sendMail(String recipient,
                     String subject,
                     String emailHtmlPattern,
                     String link);

    boolean isValidEmailAddress(String email);
}
