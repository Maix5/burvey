package lt.bleizitsoft.burvey.EmailService;

import lt.bleizitsoft.burvey.EmailService.api.IEmailer;
import lt.bleizitsoft.burvey.api.emailService.ILinkEmailer;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Monika.Kelpsaite on 5/19/2017.
 */
@Alternative
@ApplicationScoped
public class LinkEmailer implements ILinkEmailer{

    @Inject
    private IEmailer emailer;

    public boolean sendLinkMail(String recipient, String subject, String emailHtmlPattern, String surveyLink) {
        StringBuilder contentBuilder = new StringBuilder();
        try {
            BufferedReader in = new BufferedReader(new FileReader(emailHtmlPattern));
            String str;
            while ((str = in.readLine()) != null) {
                contentBuilder.append(str);
            }
            in.close();
        } catch (IOException ignored) {
        }
        String content = contentBuilder.toString();

        return emailer.sendMail(recipient, subject, content, surveyLink);
    }

    public boolean isValidEmailAddress(String email) {
        return emailer.isValidEmailAddress(email);
    }
}
