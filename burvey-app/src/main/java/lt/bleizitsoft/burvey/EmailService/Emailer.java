package lt.bleizitsoft.burvey.EmailService;

import jdk.nashorn.internal.runtime.logging.Logger;
import lt.bleizitsoft.burvey.EmailService.api.AbstractEmailer;
import lt.bleizitsoft.burvey.api.log.LogBusinessLogicInterceptorBinding;
import org.apache.deltaspike.core.api.config.PropertyFileConfig;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Specializes;
import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * Created by Monika.Kelpsaite on 5/25/2017.
 */
@ApplicationScoped
@Alternative
@Logger
public class Emailer extends AbstractEmailer{
    private String SMTP_HOST = "smtp.gmail.com";
    private String PASSWORD;
    private String FROM_ADDRESS;
    private String FROM_NAME;
    private final String dir = System.getProperty("user.dir");

    public Emailer(){
        Properties prop = getProperties();
        PASSWORD = prop.getProperty("gmailPassword", "");
        FROM_ADDRESS = prop.getProperty("fromAddress","");
        FROM_NAME = prop.getProperty("fromName","");

    }

    public boolean sendMail(String recipient, String subject, String emailHtmlPattern, String link) {
        try {
            emailHtmlPattern = emailHtmlPattern.replaceAll("surveyLink", link);

            Properties props = new Properties();
            props.put("mail.smtp.host", SMTP_HOST);
            props.put("mail.debug", "false");
            props.put("mail.smtp.socketFactory.port", "465"); //SSL Port
            props.put("mail.smtp.socketFactory.class",
                    "javax.net.ssl.SSLSocketFactory"); //SSL Factory Class
            props.put("mail.smtp.auth", "true"); //Enabling SMTP Authentication
            props.put("mail.smtp.port", "465"); //SMTP Port

            Session session = Session.getInstance(props, new Emailer.SocialAuth());
            MimeMessage msg = new MimeMessage(session);

            InternetAddress from = new InternetAddress(FROM_ADDRESS, FROM_NAME);
            msg.setFrom(from);

            InternetAddress toAddresses = new InternetAddress(recipient);
            msg.setRecipient(Message.RecipientType.TO, toAddresses);

            msg.setSubject(subject, "UTF-8");
            msg.setContent(emailHtmlPattern, "text/html");
            Transport.send(msg);
            return true;
        } catch (UnsupportedEncodingException ex) {
            return false;

        } catch (MessagingException ex) {
            return false;
        }
    }

    @LogBusinessLogicInterceptorBinding
    private Properties getProperties() {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            String filename = "config.properties";
            input = getClass().getClassLoader().getResourceAsStream(filename);
            if(input==null){
                return null;
            }

            //load a properties file from class path, inside static method
            prop.load(input);
        } catch (IOException ex) {
            return null;
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    return null;
                }
            }
        }
        return prop;
    }

    class SocialAuth extends Authenticator {

        @Override
        protected PasswordAuthentication getPasswordAuthentication() {

            return new PasswordAuthentication(FROM_ADDRESS, PASSWORD);

        }
    }
}
