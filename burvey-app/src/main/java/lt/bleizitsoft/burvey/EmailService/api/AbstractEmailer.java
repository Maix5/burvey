package lt.bleizitsoft.burvey.EmailService.api;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 * Created by marius on 5/31/17.
 */
public abstract class AbstractEmailer implements IEmailer {

    @Override
    public boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }
}
