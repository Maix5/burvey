package lt.bleizitsoft.burvey.interceptors.authentication;

import lt.bleizitsoft.burvey.api.authCheck.LoggedIn;
import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;
import lt.bleizitsoft.burvey.api.staticobjects.PagePaths;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.io.Serializable;

/**
 * Created by marius on 5/29/17.
 */
@LoggedIn
@Interceptor
public class LoggedInInterceptorImpl implements Serializable{

    @Inject private IAuthenticatedUserHolder authenticatedUserHolder;

    /**
     * Checks if method was called by 'Registered User'.
     * If FALSE redirects to 'login.xhtml' page.
     */
    @AroundInvoke
    public Object redirectIfAnonymousUser(InvocationContext ctx)
            throws Exception {

        return GeneralAuthChecker.execCheck(
                PagePaths.LOGIN_LINK,
                !authenticatedUserHolder.isUser(),
                ctx);

    }
}
