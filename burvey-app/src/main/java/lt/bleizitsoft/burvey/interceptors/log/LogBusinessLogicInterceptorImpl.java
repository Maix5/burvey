package lt.bleizitsoft.burvey.interceptors.log;

import lombok.extern.slf4j.Slf4j;
import lt.bleizitsoft.burvey.api.staticobjects.Constances;
import lt.bleizitsoft.burvey.api.staticobjects.UserGroupsType;
import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;
import lt.bleizitsoft.burvey.api.log.LogBusinessLogicInterceptorBinding;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by marius on 5/22/17.
 *
 * Implementation of Business logic logging interceptor.
 */
@LogBusinessLogicInterceptorBinding
@Interceptor
@Slf4j
public class LogBusinessLogicInterceptorImpl implements Serializable {

    @Inject private IAuthenticatedUserHolder authenticatedUserHolder;

    private String getUserEmail(){
        return authenticatedUserHolder.getMaybeAuthenticatedUserProfile()
                .map(x -> x.getCandidateInfo().getEmailAddress())
                .orElse("");
    }
    private String getUserGroup(){
        return authenticatedUserHolder.getMaybeAuthenticatedUserProfile()
                .map(x ->
                        (x.getAdministrator())
                            ? UserGroupsType.ADMINISTRATOR_USER.toString()
                            : UserGroupsType.REGISTERED_USER.toString())
                .orElse(UserGroupsType.ANONYMOUS_USER.toString());
    }


    @AroundInvoke
    public Object logBusinessLogicAction(InvocationContext ctx)
            throws Exception {

        String userEmail = getUserEmail();
        String userGroup = getUserGroup();

        DateFormat dateFormat =
                new SimpleDateFormat(Constances.SIMPLE_DATETIME_FORMAT);
        Date date = new Date();

        String className = ctx.getMethod().getDeclaringClass().getName();
        String methodName = ctx.getMethod().getName();

        StringBuilder builder = new StringBuilder(0);
        builder.append(String.format("\"%s\" -> ", dateFormat.format(date)));
        builder.append(String.format("UserEmail: \"%s\", ", userEmail));
        builder.append(String.format("Group: \"%s\", ", userGroup));
        builder.append("\n\tExecutes: ");
        builder.append(String.format("Class: \"%s\", ", className));
        builder.append(String.format("Method: \"%s\", ", methodName));
        builder.append(String.format("Parameters: \"%s\".",
                Arrays.toString(ctx.getParameters())));

        String message = builder.toString();

        log.info(String.format("dao log: Executing: %s", message));
        try {
            Object result = ctx.proceed();
            log.info(String.format("Execution success:\n%s", message));
            return result;
        }
        catch (Exception e) {
            log.info(String.format("Execution fail:\n%s", message));
            log.error(e.toString());
            throw e;
        }
    }
}
