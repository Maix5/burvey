package lt.bleizitsoft.burvey.interceptors.authentication;

import lt.bleizitsoft.burvey.api.authCheck.AdministratorOnly;
import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;
import lt.bleizitsoft.burvey.api.staticobjects.PagePaths;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.io.Serializable;

/**
 * Created by marius on 5/29/17.
 */
@AdministratorOnly
@Interceptor
public class AdministratorOnlyInterceptorImpl
        implements Serializable{

    @Inject private IAuthenticatedUserHolder authenticatedUserHolder;

    /**
     * Checks if method was called by 'Administrator User'.
     * If 'Registered && not Administrator' redirects to 'index.xhtml' page.
     * If 'Anonymous' redirects to 'login.xhtml' page.
     */
    @AroundInvoke
    public Object redirectIfAdministrator(InvocationContext ctx)
            throws Exception {

        return GeneralAuthChecker.execCheck(
                PagePaths.INDEX_LINK,
                !authenticatedUserHolder.isAdministrator(),
                ctx);
    }
}
