package lt.bleizitsoft.burvey.interceptors.redirect;

import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;
import lt.bleizitsoft.burvey.api.redirects.RedirectAdministratorOnlyInterceptorBinding;



import lt.bleizitsoft.burvey.api.staticobjects.PagePaths;
import org.omnifaces.util.Faces;


import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.io.Serializable;

/**
 * Created by marius on 5/29/17.
 */
@RedirectAdministratorOnlyInterceptorBinding
@Interceptor
public class RedirectAdministratorOnlyInterceptorImpl
        implements Serializable{

    @Inject private IAuthenticatedUserHolder authenticatedUserHolder;

    /**
     * Checks if method was called by 'Administrator User'.
     * If TRUE redirects to 'index.xhtml' page.
     */
    @AroundInvoke
    public Object redirectIfAdministrator(InvocationContext ctx)
            throws Exception {

        Object result = null;

        if (authenticatedUserHolder.isAdministrator()) {
            Faces.redirect(PagePaths.INDEX_LINK);
        } else {
            result = ctx.proceed();
        }

        return result;
    }
}
