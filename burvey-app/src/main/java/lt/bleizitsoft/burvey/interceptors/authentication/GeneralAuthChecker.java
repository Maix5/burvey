package lt.bleizitsoft.burvey.interceptors.authentication;

import lt.bleizitsoft.burvey.api.utils.Redirector;

import javax.interceptor.InvocationContext;

/**
 * Created by marius on 5/30/17.
 */
class GeneralAuthChecker {

    static Object execCheck(String redirectToPage,
                             boolean userCheck,
                             InvocationContext ctx)
            throws Exception{

        if (userCheck) {
            Redirector.redirectNoExeception(
                    redirectToPage,
                    Throwable::printStackTrace);
            return null;
        }

        return ctx.proceed();
    }
}
