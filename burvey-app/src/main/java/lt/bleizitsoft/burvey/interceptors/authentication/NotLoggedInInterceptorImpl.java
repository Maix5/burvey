package lt.bleizitsoft.burvey.interceptors.authentication;

import lt.bleizitsoft.burvey.api.authCheck.NotLoggedIn;
import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;
import lt.bleizitsoft.burvey.api.staticobjects.PagePaths;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.io.Serializable;

/**
 * Created by marius on 5/29/17.
 */
@NotLoggedIn
@Interceptor
public class NotLoggedInInterceptorImpl implements Serializable{

    @Inject private IAuthenticatedUserHolder authenticatedUserHolder;

    /**
     * Checks if method was called by 'Anonymous User'.
     * If FALSE redirects to 'index.xhtml' page.
     */
    @AroundInvoke
    public Object redirectRegisteredUser(InvocationContext ctx)
            throws Exception {

        return GeneralAuthChecker.execCheck(
                PagePaths.INDEX_LINK,
                !authenticatedUserHolder.isAnonymousUser(),
                ctx);
    }
}
