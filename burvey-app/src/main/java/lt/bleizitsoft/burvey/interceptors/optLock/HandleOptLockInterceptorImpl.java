package lt.bleizitsoft.burvey.interceptors.optLock;

import lombok.extern.slf4j.Slf4j;
import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;
import lt.bleizitsoft.burvey.api.log.LogBusinessLogicInterceptorBinding;
import lt.bleizitsoft.burvey.api.optLock.HandleOptLock;
import lt.bleizitsoft.burvey.api.staticobjects.Constances;
import lt.bleizitsoft.burvey.api.staticobjects.InfoMessages;
import lt.bleizitsoft.burvey.api.staticobjects.UserGroupsType;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.OptimisticLockException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by marius on 5/22/17.
 *
 * Implementation of Business logic logging interceptor.
 */
@HandleOptLock
@Interceptor
@Slf4j
public class HandleOptLockInterceptorImpl implements Serializable {

    @AroundInvoke
    public Object handleOptLockAction(InvocationContext ctx)
            throws Exception {

        Object result = null;

        try {
            result = ctx.proceed();
        }
        catch (OptimisticLockException e) {
            InfoMessages.clearMessages();
            InfoMessages.setErrorMessage(
                    "Atsiprašome," +
                    " bet jūsų pakeitimai nebuvo išsaugoti," +
                    " nes įvyko saugomų duomenų konfliktas." +
                    " Pabandykite atlikti veiksmą iš naujo.");

            log.error(e.getMessage());
        }

        return result;
    }
}
