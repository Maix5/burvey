package lt.bleizitsoft.burvey.dao.sync;

import lt.bleizitsoft.burvey.api.dao.AbstractNumberQuestionDAO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * Created by Monika.Kelpsaite on 5/14/2017.
 */
@ApplicationScoped
public class NumberQuestionDAO extends AbstractNumberQuestionDAO {


    @Inject
    EntityManager em;

    @Override
    protected EntityManager getEm() {
        return em;
    }
}
