/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.dao.sync;

import lt.bleizitsoft.burvey.api.dao.AbstractUserMultipleChoiceAnswerOptionDAO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author marius
 */
@ApplicationScoped
public class UserMultipleChoiceAnswerOptionDAO
        extends AbstractUserMultipleChoiceAnswerOptionDAO {


    @Inject
    EntityManager em;

    @Override
    protected EntityManager getEm() {
        return em;
    }
}
