/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.dao.async;

import lt.bleizitsoft.burvey.api.async.RescueOrAsync;
import lt.bleizitsoft.burvey.api.dao.AbstractUserMultipleChoiceAnswerOptionDAO;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 *
 * @author marius
 */
@Dependent
@RescueOrAsync
public class AsyncUserMultipleChoiceAnswerOptionDAO
        extends AbstractUserMultipleChoiceAnswerOptionDAO  implements Serializable {


    @Inject
    @RescueOrAsync
    EntityManager em;

    @Override
    protected EntityManager getEm() {
        return em;
    }
}
