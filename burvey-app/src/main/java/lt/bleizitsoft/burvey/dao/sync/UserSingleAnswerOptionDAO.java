package lt.bleizitsoft.burvey.dao.sync;

import lt.bleizitsoft.burvey.api.dao.AbstractUserSingleAnswerOptionDAO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * Created by Monika.Kelpsaite on 5/14/2017.
 */
@ApplicationScoped
public class UserSingleAnswerOptionDAO
        extends AbstractUserSingleAnswerOptionDAO {


    @Inject
    EntityManager em;

    @Override
    protected EntityManager getEm() {
        return em;
    }
}
