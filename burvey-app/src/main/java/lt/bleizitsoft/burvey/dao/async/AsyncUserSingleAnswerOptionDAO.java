package lt.bleizitsoft.burvey.dao.async;

import lt.bleizitsoft.burvey.api.async.RescueOrAsync;
import lt.bleizitsoft.burvey.api.dao.AbstractUserSingleAnswerOptionDAO;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * Created by Monika.Kelpsaite on 5/14/2017.
 */
@Dependent
@RescueOrAsync
public class AsyncUserSingleAnswerOptionDAO
        extends AbstractUserSingleAnswerOptionDAO  implements Serializable {


    @Inject
    @RescueOrAsync
    EntityManager em;

    @Override
    protected EntityManager getEm() {
        return em;
    }
}
