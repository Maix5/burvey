package lt.bleizitsoft.burvey.dao.sync;

import lt.bleizitsoft.burvey.api.dao.AbstractUserSurveyAnswerGroupDAO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * Created by marius on 5/23/17.
 */
@ApplicationScoped
public class UserSurveyAnswerGroupDAO extends AbstractUserSurveyAnswerGroupDAO {


    @Inject
    EntityManager em;

    @Override
    protected EntityManager getEm() {
        return em;
    }
}
