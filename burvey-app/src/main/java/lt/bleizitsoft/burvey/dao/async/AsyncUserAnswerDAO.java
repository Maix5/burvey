package lt.bleizitsoft.burvey.dao.async;

import lt.bleizitsoft.burvey.api.async.RescueOrAsync;
import lt.bleizitsoft.burvey.api.dao.AbstractUserAnswerDAO;
import lt.bleizitsoft.burvey.entities.UserAnswer;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * Created by Monika.Kelpsaite on 5/14/2017.
 */
@Dependent
@RescueOrAsync
public class AsyncUserAnswerDAO extends AbstractUserAnswerDAO implements Serializable {
    @Override
    public void create(UserAnswer obj) {
        super.create(obj);
    }

    @Override
    public void update(UserAnswer obj) {
        super.update(obj);
    }

    @Inject
    @RescueOrAsync
    EntityManager em;

    @Override
    protected EntityManager getEm() {
        return em;
    }
}