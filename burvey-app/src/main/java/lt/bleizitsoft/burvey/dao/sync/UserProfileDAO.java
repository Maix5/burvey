package lt.bleizitsoft.burvey.dao.sync;

import lt.bleizitsoft.burvey.api.dao.AbstractUserProfileDAO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * Created by marius on 5/23/17.
 */
@ApplicationScoped
public class UserProfileDAO extends AbstractUserProfileDAO {


    @Inject
    EntityManager em;

    @Override
    protected EntityManager getEm() {
        return em;
    }
}
