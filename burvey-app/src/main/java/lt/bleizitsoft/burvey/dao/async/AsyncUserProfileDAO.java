package lt.bleizitsoft.burvey.dao.async;

import lt.bleizitsoft.burvey.api.async.RescueOrAsync;
import lt.bleizitsoft.burvey.api.dao.AbstractUserProfileDAO;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * Created by marius on 5/23/17.
 */
@Dependent
@RescueOrAsync
public class AsyncUserProfileDAO extends AbstractUserProfileDAO implements Serializable {


    @Inject
    @RescueOrAsync
    EntityManager em;

    @Override
    protected EntityManager getEm() {
        return em;
    }
}
