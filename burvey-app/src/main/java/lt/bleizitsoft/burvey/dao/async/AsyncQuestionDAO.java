package lt.bleizitsoft.burvey.dao.async;

import lt.bleizitsoft.burvey.api.async.RescueOrAsync;
import lt.bleizitsoft.burvey.api.dao.AbstractQuestionDAO;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * Created by Monika.Kelpsaite on 5/7/2017.
 */
@Dependent
@RescueOrAsync
public class AsyncQuestionDAO extends AbstractQuestionDAO  implements Serializable {


    @Inject
    @RescueOrAsync
    EntityManager em;

    @Override
    protected EntityManager getEm() {
        return em;
    }
}
