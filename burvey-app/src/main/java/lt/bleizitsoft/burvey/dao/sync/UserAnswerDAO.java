package lt.bleizitsoft.burvey.dao.sync;

import lt.bleizitsoft.burvey.api.dao.AbstractUserAnswerDAO;
import lt.bleizitsoft.burvey.entities.UserAnswer;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * Created by Monika.Kelpsaite on 5/14/2017.
 */
@ApplicationScoped
public class UserAnswerDAO extends AbstractUserAnswerDAO {

    @Inject
    EntityManager em;

    @Override
    protected EntityManager getEm() {
        return em;
    }


    @Override
    public void create(UserAnswer obj) {
        em.persist(obj);
    }
}