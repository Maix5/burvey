package lt.bleizitsoft.burvey.dao.sync;

import lt.bleizitsoft.burvey.api.dao.AbstractSurveyDAO;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 * Created by Monika.Kelpsaite on 5/7/2017.
 */
@ApplicationScoped
public class SurveyDAO extends AbstractSurveyDAO {


    @Inject
    EntityManager em;

    @Override
    protected EntityManager getEm() {
        return em;
    }
}
