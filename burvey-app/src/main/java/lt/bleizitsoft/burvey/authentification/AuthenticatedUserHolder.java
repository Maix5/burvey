package lt.bleizitsoft.burvey.authentification;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import lt.bleizitsoft.burvey.api.dao.ICandidateInfoDAO;
import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;
import lt.bleizitsoft.burvey.authentification.crypto.BCrypt;
import lt.bleizitsoft.burvey.entities.UserProfile;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by marius on 5/23/17.
 */
@Named
@SessionScoped
public class AuthenticatedUserHolder implements IAuthenticatedUserHolder {

    @Inject
    private ICandidateInfoDAO candidateInfoDao;

    @Getter
    private UserProfile authenticatedUserProfile;

    @Override
    public void initUser(String userEmailAddress, String userPassword) {
        updateAuthenticatedUserProfile(userEmailAddress);
    }

    @Override
    public void updateAuthenticatedUserProfile(String email) {
        candidateInfoDao.readByColumnEmailAddress(email)
                .ifPresent(x -> authenticatedUserProfile = x.getUserProfile());
    }

    @Override
    public void updateAuthenticatedUserProfile(UserProfile userProfile) {
        authenticatedUserProfile = userProfile;
    }

    @Override
    public void updateAuthenticatedUserProfile() {
        authenticatedUserProfile = null;
    }
}
