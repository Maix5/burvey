package lt.bleizitsoft.burvey.authentification;

import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;
import lt.bleizitsoft.burvey.authentification.crypto.BCrypt;
import lt.bleizitsoft.burvey.entities.UserProfile;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.enterprise.inject.Any;
import javax.inject.Inject;
import java.io.Serializable;

/**
 * Created by marius on 5/23/17.
 */
@Decorator
public class AuthorizedUserHolderDecorator
        implements IAuthenticatedUserHolder, Serializable {

    @Inject @Delegate @Any
    private IAuthenticatedUserHolder authenticatedUserHolder;


    @Override
    public UserProfile getAuthenticatedUserProfile() {
        return authenticatedUserHolder.getAuthenticatedUserProfile();
    }

    @Override
    public void initUser(String userEmailAddress, String userPassword) {

        authenticatedUserHolder.initUser(userEmailAddress, userPassword);
        authenticatedUserHolder.getMaybeAuthenticatedUserProfile()
                .ifPresent(authenticatedUserHolder ->
                    updateAuthenticatedUserProfile(
                            BCrypt.checkpw(
                                    userPassword,
                                    authenticatedUserHolder.getHashedPassword())
                                ? authenticatedUserHolder : null));

    }

    @Override
    public void updateAuthenticatedUserProfile(String email) {
        authenticatedUserHolder.updateAuthenticatedUserProfile(email);
    }

    @Override
    public void updateAuthenticatedUserProfile(UserProfile userProfile) {
        authenticatedUserHolder.updateAuthenticatedUserProfile(userProfile);
    }
}
