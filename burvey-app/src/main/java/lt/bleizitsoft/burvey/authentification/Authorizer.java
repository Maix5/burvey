package lt.bleizitsoft.burvey.authentification;

import lt.bleizitsoft.burvey.api.authentification.IAuthenticatedUserHolder;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

/**
 * Useful if delta spike security utility is used.
 *
 * Created by marius on 5/23/17.
 */
@ApplicationScoped
public class Authorizer {

//    @Inject
//    private IAuthenticatedUserHolder authenticatedUserHolder;

}
