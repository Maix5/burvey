package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.NumberQuestion;
import lt.bleizitsoft.burvey.entities.UserAnswer;
import lt.bleizitsoft.burvey.entities.UserAnswerNumber;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Monika.Kelpsaite on 5/14/2017.
 */
public abstract class AbstractNumberQuestionDAO
        extends AbstractBaseDAO<NumberQuestion>
        implements INumberQuestionDAO {

    @Override
    public List<NumberQuestion> readByColumnMinimalRange(Integer minimalRange) {
        return getSingleParamQueryEntityList(
                "NumberQuestion.findByMinimalRange",
                "minimalRange", minimalRange);
    }

    @Override
    public List<NumberQuestion> readByColumnMaximumRange(Integer maximumRange) {
        return getSingleParamQueryEntityList(
                "NumberQuestion.findByMaximumRange",
                "maximumRange", maximumRange);
    }

    @Override
    public List<UserAnswerNumber> readUserAnswerNumberByNumberQuestionId
            (Integer numberQuestionId) {
        return readById(numberQuestionId)
                .map(numberQuestion ->
                        numberQuestion
                                .getQuestion()
                                .getUserAnswerList()
                                .stream()
                                .map(UserAnswer::getUserAnswerNumber)
                                .collect(Collectors.toList()))
                .orElse(new ArrayList<>(0));
    }

    @Override
    protected Class<NumberQuestion> getEntityClassType() {
        return NumberQuestion.class;
    }
    @Override
    protected String getReadAllQueryName() {
        return "NumberQuestion.findAll";
    }
}
