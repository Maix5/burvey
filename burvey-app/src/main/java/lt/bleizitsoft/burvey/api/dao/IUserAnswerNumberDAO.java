package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.UserAnswerNumber;

import java.util.List;

/**
 * Created by marius on 5/23/17.
 */
public interface IUserAnswerNumberDAO
        extends IBaseDAO<UserAnswerNumber> {

    List<UserAnswerNumber> readByColumnContentNumber(Integer contentNumber);
}
