package lt.bleizitsoft.burvey.api.utils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Albe on 2017-05-28.
 */
public abstract class UrlParser {

    public static List<String> getBaseUrl(HttpServletRequest request) {
        String queryParamName = request.getQueryString() != null ? request.getQueryString().split("=")[0] : null;
        String accessLink = request.getQueryString() != null ? request.getQueryString().split("=")[1] : null;
        List<String> result = new ArrayList<>();
        if(queryParamName != null && accessLink != null) {
            result.add(queryParamName);
            result.add(accessLink);
        }
        return result;
    }

}
