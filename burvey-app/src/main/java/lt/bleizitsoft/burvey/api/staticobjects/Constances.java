package lt.bleizitsoft.burvey.api.staticobjects;

/**
 * Created by marius on 5/27/17.
 */
public interface Constances {

    final static String SIMPLE_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
}
