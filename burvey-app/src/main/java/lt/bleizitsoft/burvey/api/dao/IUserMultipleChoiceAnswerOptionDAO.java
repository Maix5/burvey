package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.UserMultipleChoiceAnswerOption;

/**
 * Created by marius on 5/23/17.
 */
public interface IUserMultipleChoiceAnswerOptionDAO
        extends IBaseDAO<UserMultipleChoiceAnswerOption> {
}
