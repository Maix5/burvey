package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.CandidateInfo;
import lt.bleizitsoft.burvey.entities.UserProfile;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Created by marius on 5/23/17.
 */
public abstract class AbstractUserProfileDAO
        extends AbstractBaseDAO<UserProfile>
        implements IUserProfileDAO {

    @Inject
    private ICandidateInfoDAO candidateInfoDao;

    @Override
    public List<UserProfile> readByColumnName(String name) {
        return getSingleParamQueryEntityList(
                "UserProfile.findByName", "name", name);
    }

    @Override
    public List<UserProfile> readByColumnSurname(String surname) {
        return getSingleParamQueryEntityList(
                "UserProfile.findBySurname", "surname", surname);
    }

    @Override
    public List<UserProfile> readByNameSurname(String name, String surname) {
        return getEm()
                .createNamedQuery(
                        "UserProfile.findByNameSurname",
                        getEntityClassType())
                .setParameter("name", name)
                .setParameter("surname", surname)
                .getResultList();
    }

    @Override
    public Optional<UserProfile> readByCandidateInfoId(
            Integer candidateInfoId) {
        return candidateInfoDao.readById(candidateInfoId)
                .map(CandidateInfo::getUserProfile);
    }

    @Override
    public List<UserProfile> readByColumnHashedPassword(String hashedPassword) {
        return getSingleParamQueryEntityList(
                "UserProfile.findByHashedPassword",
                "hashedPassword", hashedPassword);
    }

    @Override
    public List<UserProfile> readByColumnAdministrator(Boolean administrator) {
        return getSingleParamQueryEntityList(
                "UserProfile.findByAdministrator",
                "administrator", administrator);
    }

    @Override
    public List<UserProfile> readByColumnBlocked(Boolean blocked) {
        return getSingleParamQueryEntityList(
                "UserProfile.findByBlocked", "blocked", blocked);
    }


    @Override
    protected Class<UserProfile> getEntityClassType() {
        return UserProfile.class;
    }
    @Override
    protected String getReadAllQueryName() {
        return "UserProfile.findAll";
    }
}
