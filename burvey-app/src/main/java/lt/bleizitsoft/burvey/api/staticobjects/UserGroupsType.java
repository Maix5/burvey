package lt.bleizitsoft.burvey.api.staticobjects;

/**
 * Created by marius on 5/23/17.
 */
public enum UserGroupsType {
    ANONYMOUS_USER,
    REGISTERED_USER,
    ADMINISTRATOR_USER;

    private final static String administrator  = "ADMINISTRATOR_USER";
    private final static String registeredUser = "REGISTERED_USER";
    private final static String anonymousUser  = "ANONYMOUS_USER";


    @Override
    public String toString() {
        switch (this){
            case ADMINISTRATOR_USER:
                return administrator;
            case REGISTERED_USER:
                return registeredUser;
            default:
                return anonymousUser;
        }
    }
}
