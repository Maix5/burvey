package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.AnswersAccessRegistry;
import lt.bleizitsoft.burvey.entities.UserSurveyAnswerGroup;

import java.util.Optional;

/**
 * Created by marius on 5/23/17.
 */
public abstract class AbstractUserSurveyAnswerGroupDAO
        extends AbstractBaseDAO<UserSurveyAnswerGroup>
        implements IUserSurveyAnswerGroupDAO {
    @Override
    protected Class<UserSurveyAnswerGroup> getEntityClassType() {
        return UserSurveyAnswerGroup.class;
    }

    @Override
    protected String getReadAllQueryName() {
        return "UserSurveyAnswerGroup.findAll";
    }

    public Optional<UserSurveyAnswerGroup> getUserSurveyAccessGroupByAccessRegistry(AnswersAccessRegistry answersAccessRegistry) {
        return getSingleParamQuerySingleEntity(
                "UserSurveyAnswerGroup.findByAccessRegistryId",
                "answersAccessRegistry", answersAccessRegistry);
    }
}
