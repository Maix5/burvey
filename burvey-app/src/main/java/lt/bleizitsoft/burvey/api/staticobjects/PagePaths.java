package lt.bleizitsoft.burvey.api.staticobjects;

/**
 * Created by Gintautas.Grisius on 2017-05-24.
 */
public interface PagePaths {

    String BASE_PATH = "http://localhost:8080";
    String INDEX_LINK = "/index";
    String ANSWER_SURVEY_LINK = "/answer_survey";
    String CREATE_SURVEY_LINK = "/create_survey";
    String LOGIN_LINK = "/login";
    String SURVEY_IMPORT = "/import_survey";
    String REGISTER_LINK = "/register";
    String USER_MANAGEMENT = "/user_managament";
    String REPORT_LINK = "/survey_report";
    String FORGOT_LINK = "/forgot_password";
    String RECOVER_LINK = "/reset";

}
