package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.Question;

import java.util.List;

/**
 * Created by Monika.Kelpsaite on 5/7/2017.
 */
public abstract class AbstractQuestionDAO
        extends AbstractBaseDAO<Question>
        implements IQuestionDAO {

    @Override
    public List<Question> readByColumnSurveyId(Integer surveyId) {
        return getSingleParamQueryEntityList(
                "Question.findBySurveyId", "surveyId", surveyId);
    }

    @Override
    public List<Question> readByColumnQuestionType(String questionType) {
        return getSingleParamQueryEntityList(
                "Question.findByQuestionType", "questionType", questionType);
    }

    @Override
    public List<Question> readByColumnContent(String content) {
        return getSingleParamQueryEntityList(
                "Question.findByContent", "content", content);
    }

    @Override
    public List<Question> readByColumnOrderNr(Integer orderNr) {
        return getSingleParamQueryEntityList(
                "Question.findByOrderNr", "orderNr", orderNr);
    }

    @Override
    public List<Question> readByColumnRequired(Boolean required) {
        return getSingleParamQueryEntityList(
                "Question.findByRequired", "required", required);
    }


    @Override
    protected Class<Question> getEntityClassType() {
        return Question.class;
    }
    @Override
    protected String getReadAllQueryName() {
        return "Question.findAll";
    }
}
