package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.Survey;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by marius on 5/23/17.
 */
public interface ISurveyDAO
        extends IBaseDAO<Survey> {

    Optional<Survey> readByColumnAccessLink(String accessLink);
    List<Survey> readByColumnTitle(String title);
    List<Survey> readByColumnDescription(String description);
    List<Survey> readByColumnActivationDate(Date activationDate);
    List<Survey> readByColumnExpirationDate(Date expirationDate);
    List<Survey> readByColumnIsActive(Boolean isActive);
    List<Survey> readByColumnIsPublic(Boolean isPublic);
    List<Survey> readByColumnQuestionsPerPage(Integer questionsPerPage);
}
