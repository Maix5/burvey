package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.CandidateInfo;
import lt.bleizitsoft.burvey.entities.UserProfile;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by marius on 5/23/17.
 */
public interface IUserProfileDAO extends IBaseDAO<UserProfile> {

    List<UserProfile> readByColumnName(String name);
    List<UserProfile> readByColumnSurname(String surname);

    default List<UserProfile> readByNameSurname(String name, String surname){
        return readByColumnName(name).stream()
                .filter(x -> x.getSurname().equals(surname))
                .collect(Collectors.toList());
    }

    Optional<UserProfile> readByCandidateInfoId(Integer candidateInfoId);
    default Optional<UserProfile> readByCandidateInfo(
            CandidateInfo candidateInfo){
        return readByCandidateInfoId(candidateInfo.getId());
    }

    List<UserProfile> readByColumnHashedPassword(String hashedPassword);
    List<UserProfile> readByColumnAdministrator(Boolean administrator);
    List<UserProfile> readByColumnBlocked(Boolean blocked);
}
