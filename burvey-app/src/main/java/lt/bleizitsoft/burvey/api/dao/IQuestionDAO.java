package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.api.staticobjects.SurveyQuestionType;
import lt.bleizitsoft.burvey.entities.Question;

import java.util.List;

/**
 * Created by marius on 5/23/17.
 */
public interface IQuestionDAO
        extends IBaseDAO<Question> {

    List<Question> readByColumnSurveyId(Integer surveyId);
    List<Question> readByColumnQuestionType(String questionType);
    default List<Question> readByColumnQuestionType(
            SurveyQuestionType QuestionType){
        return readByColumnQuestionType(QuestionType.toString());
    }
    List<Question> readByColumnContent(String content);
    List<Question> readByColumnOrderNr(Integer orderNr);
    List<Question> readByColumnRequired(Boolean required);
}
