package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.UserSingleAnswerOption;

/**
 * Created by Monika.Kelpsaite on 5/14/2017.
 */
public abstract class AbstractUserSingleAnswerOptionDAO
        extends AbstractBaseDAO<UserSingleAnswerOption>
        implements IUserSingleAnswerOptionDAO {

    @Override
    protected Class<UserSingleAnswerOption> getEntityClassType() {
        return UserSingleAnswerOption.class;
    }
    @Override
    protected String getReadAllQueryName() {
        return "UserSingleAnswerOption.findAll";
    }
}
