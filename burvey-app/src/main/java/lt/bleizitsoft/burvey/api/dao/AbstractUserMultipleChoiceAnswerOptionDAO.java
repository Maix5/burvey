/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.UserMultipleChoiceAnswerOption;

/**
 *
 * @author marius
 */
public abstract class AbstractUserMultipleChoiceAnswerOptionDAO
        extends AbstractBaseDAO<UserMultipleChoiceAnswerOption>
        implements IUserMultipleChoiceAnswerOptionDAO {

    @Override
    protected Class<UserMultipleChoiceAnswerOption> getEntityClassType() {
        return UserMultipleChoiceAnswerOption.class;
    }

    @Override
    protected String getReadAllQueryName() {
        return "UserMultipleChoiceAnswerOption.findAll";
    }
}
