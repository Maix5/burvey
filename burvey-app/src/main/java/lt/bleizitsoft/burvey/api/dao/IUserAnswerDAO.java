package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.UserAnswer;

/**
 * Created by marius on 5/23/17.
 */
public interface IUserAnswerDAO extends IBaseDAO<UserAnswer> {
}
