package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.UserSingleAnswerOption;

/**
 * Created by marius on 5/23/17.
 */
public interface IUserSingleAnswerOptionDAO
        extends IBaseDAO<UserSingleAnswerOption> {
}
