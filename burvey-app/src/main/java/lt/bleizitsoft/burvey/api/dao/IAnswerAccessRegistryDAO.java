package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.AnswersAccessRegistry;

import java.util.Optional;

/**
 * Created by marius on 5/23/17.
 */
public interface IAnswerAccessRegistryDAO
        extends IBaseDAO<AnswersAccessRegistry> {

    Optional<AnswersAccessRegistry> readByColumnAccessLink(String accessLink);
}
