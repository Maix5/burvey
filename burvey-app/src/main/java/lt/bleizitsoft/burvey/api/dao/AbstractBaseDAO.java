package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.api.optLock.HandleOptLock;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

/**
 * Created by marius on 5/23/17.
 */
public abstract class AbstractBaseDAO<Entity>
        implements IBaseDAO<Entity> {

    @Override
    public List<Entity> readAll() {
        return getEm()
                .createNamedQuery(getReadAllQueryName(), getEntityClassType())
                .getResultList();
    }

    @Override
    public Optional<Entity> readById(Integer id) {
        return Optional.ofNullable(getEm().find(getEntityClassType(), id));
    }

    @Override
    public void create(Entity obj) {
        getEm().persist(obj);
    }

    @HandleOptLock
    @Override
    public void update(Entity obj) {
        getEm().merge(obj);
        getEm().flush();
    }

    @Override
    public void deleteById(Integer id) {
        getEm().remove(readById(id).orElse(null));
        getEm().flush();
    }

    @Override
    public void delete(Entity obj) {
        getEm().remove(obj);
        getEm().flush();
    }

    protected Optional<Entity> getQuerySingleEntity(List<Entity> entities){
        return entities.stream().findFirst();
    }
    protected Optional<Entity> getQuerySingleEntity(
            TypedQuery<Entity> entitiesQuery){
        return getQuerySingleEntity(entitiesQuery.getResultList());
    }

    protected List<Entity> getSingleParamQueryEntityList(
            String queryName, String paramName, Object param){
        return getEm().createNamedQuery(queryName, getEntityClassType())
                .setParameter(paramName, param).getResultList();
    }
    protected Optional<Entity> getSingleParamQuerySingleEntity(
            String queryName, String paramName, Object param){
        return getQuerySingleEntity(
                getEm().createNamedQuery(queryName, getEntityClassType())
                .setParameter(paramName, param));
    }

    protected abstract Class<Entity> getEntityClassType();
    protected abstract String getReadAllQueryName();
    protected abstract EntityManager getEm();
}
