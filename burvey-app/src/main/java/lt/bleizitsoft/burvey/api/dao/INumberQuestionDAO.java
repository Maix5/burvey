package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.NumberQuestion;
import lt.bleizitsoft.burvey.entities.UserAnswerNumber;

import java.util.List;

/**
 * Created by marius on 5/23/17.
 */
public interface INumberQuestionDAO
        extends IBaseDAO<NumberQuestion> {

    List<NumberQuestion> readByColumnMinimalRange(Integer minimalRange);
    List<NumberQuestion> readByColumnMaximumRange(Integer maximumRange);


    List<UserAnswerNumber> readUserAnswerNumberByNumberQuestionId(
            Integer numberQuestionId);
    default List<UserAnswerNumber> readUserAnswerNumberByNumberQuestion(
            NumberQuestion numberQuestion){
        return readUserAnswerNumberByNumberQuestionId(numberQuestion.getId());
    }
}
