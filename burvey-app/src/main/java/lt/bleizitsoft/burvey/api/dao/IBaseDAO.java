package lt.bleizitsoft.burvey.api.dao;

import java.util.List;
import java.util.Optional;

/**
 * Created by marius on 5/23/17.
 *
 * Basic Default entity CRUD operations.
 */
public interface IBaseDAO<Entity> {

    List<Entity> readAll();
    Optional<Entity> readById(Integer id);

    void create (Entity obj);
    void update(Entity obj);
    void deleteById(Integer id);
    void delete(Entity obj);
}
