package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.AnswersAccessRegistry;
import lt.bleizitsoft.burvey.entities.UserSurveyAnswerGroup;

import java.util.Optional;

/**
 * Created by marius on 5/23/17.
 */
public interface IUserSurveyAnswerGroupDAO
        extends IBaseDAO<UserSurveyAnswerGroup> {
    Optional<UserSurveyAnswerGroup> getUserSurveyAccessGroupByAccessRegistry(AnswersAccessRegistry answersAccessRegistry);
}
