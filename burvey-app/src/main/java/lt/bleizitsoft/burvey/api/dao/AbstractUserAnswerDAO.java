package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.AnswersAccessRegistry;
import lt.bleizitsoft.burvey.entities.UserAnswer;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Monika.Kelpsaite on 5/14/2017.
 */
public abstract class AbstractUserAnswerDAO
        extends AbstractBaseDAO<UserAnswer>
        implements IUserAnswerDAO {

    @Inject
    IAnswerAccessRegistryDAO answerAccessRegistryDao;

    @Override
    protected Class<UserAnswer> getEntityClassType() {
        return UserAnswer.class;
    }
    @Override
    protected String getReadAllQueryName() {
        return "UserAnswer.findAll";
    }

    public List<UserAnswer> getUserAnswersByAnswersAccessRegistry(
            AnswersAccessRegistry answersRegistryId) {
        return answerAccessRegistryDao
                .readById(answersRegistryId.getId())
                .map(answersAccessRegistry -> answersAccessRegistry
                        .getUserSurveyAnswerGroup().getUserAnswerList())
                .orElse(new ArrayList<>(0));
    }
}