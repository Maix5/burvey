package lt.bleizitsoft.burvey.api.utils;

import org.omnifaces.util.Faces;

import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.function.Consumer;

/**
 * Created by marius on 5/30/17.
 */
public class Redirector {

    private static void retainFacessMessages(){

        // Keep all faces messages for later.
        FacesContext
                .getCurrentInstance()
                .getExternalContext()
                .getFlash()
                .setKeepMessages(true);
    }

    public static void redirect(String redirectTo) throws IOException{

        retainFacessMessages();
        Faces.redirect(redirectTo);
    }

    public static void redirectNoExeception(String redirectTo,
                                            Consumer<IOException> catchFunc) {

        retainFacessMessages();
        try {
            Faces.redirect(redirectTo);
        } catch (IOException e) {
            catchFunc.accept(e);
        }
    }
}
