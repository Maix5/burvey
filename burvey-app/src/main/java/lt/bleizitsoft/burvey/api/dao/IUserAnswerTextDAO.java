package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.UserAnswerText;

import java.util.List;

/**
 * Created by marius on 5/23/17.
 */
public interface IUserAnswerTextDAO extends IBaseDAO<UserAnswerText> {

    List<UserAnswerText> readByColumnContentText(String contentText);
}
