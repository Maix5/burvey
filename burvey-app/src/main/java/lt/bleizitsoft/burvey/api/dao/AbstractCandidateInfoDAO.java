package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.CandidateInfo;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by marius on 5/23/17.
 */
public abstract class AbstractCandidateInfoDAO
        extends AbstractBaseDAO<CandidateInfo>
        implements ICandidateInfoDAO {

    @Override
    protected Class<CandidateInfo> getEntityClassType() {
        return CandidateInfo.class;
    }
    @Override
    protected String getReadAllQueryName() {
        return "CandidateInfo.findAll";
    }


    @Override
    public Optional<CandidateInfo> readByColumnEmailAddress(
            String emailAddress) {
        return getSingleParamQuerySingleEntity(
                "CandidateInfo.findByEmailAddress",
                "emailAddress", emailAddress);
    }

    @Override
    public Optional<CandidateInfo> readByColumnPasswordRecoveryLink(
            String passwordRecoveryLink) {
        return getSingleParamQuerySingleEntity(
                "CandidateInfo.findByPasswordRecoveryLink",
                "passwordRecoveryLink", passwordRecoveryLink);
    }

    @Override
    public Optional<CandidateInfo> readByColumnActivationLink(
            String activationLink) {
        return getSingleParamQuerySingleEntity(
                "CandidateInfo.findByActivationLink",
                "activationLink", activationLink);
    }

    @Override
    public List<CandidateInfo> readByColumnActivationDate(
            Date activationDate) {
        return getSingleParamQueryEntityList(
                "CandidateInfo.findByActivationDate",
                "activationDate", activationDate);
    }

    @Override
    public List<CandidateInfo> readByColumnActivationExpirationDate(
            Date activationExpirationDate) {
        return getSingleParamQueryEntityList(
                "CandidateInfo.findByActivationExpirationDate",
                "activationExpirationDate", activationExpirationDate);
    }
}
