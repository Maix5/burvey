package lt.bleizitsoft.burvey.api.redirects;

import javax.interceptor.InterceptorBinding;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Checks if method was called by 'Registered User'. If TRUE redirects.
 *
 * Created by marius on 5/29/17.
 */
@InterceptorBinding
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RedirectRegisteredUserInterceptorBinding {
}
