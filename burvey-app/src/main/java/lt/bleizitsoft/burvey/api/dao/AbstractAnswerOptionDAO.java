package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.AnswerOption;

import java.util.List;

/**
 * Created by Monika.Kelpsaite on 5/14/2017.
 */
public abstract class AbstractAnswerOptionDAO
        extends AbstractBaseDAO<AnswerOption>
        implements IAnswerOptionDAO {

    @Override
    public List<AnswerOption> readByColumnContent(String content) {
        return getSingleParamQueryEntityList(
                "AnswerOption.findByContent", "content", content);
    }

    @Override
    protected Class<AnswerOption> getEntityClassType() {
        return AnswerOption.class;
    }
    @Override
    protected String getReadAllQueryName() {
        return "AnswerOption.findAll";
    }
}
