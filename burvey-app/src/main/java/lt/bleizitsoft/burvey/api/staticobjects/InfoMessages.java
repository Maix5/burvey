package lt.bleizitsoft.burvey.api.staticobjects;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.Iterator;

/**
 * Created by Gintautas.Grisius on 2017-05-14.
 */
public abstract class InfoMessages {

    public static void setErrorMessage(String errorMessage, String id) {
        FacesContext.getCurrentInstance().addMessage(id,
                new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, null));
    }

    public static void setInfoMessage(String infoMessage, String id) {
        FacesContext.getCurrentInstance().addMessage(id,
                new FacesMessage(FacesMessage.SEVERITY_INFO, infoMessage, null));
    }

    public static void setWarnMessage(String warnMessage, String id) {
        FacesContext.getCurrentInstance().addMessage(id,
                new FacesMessage(FacesMessage.SEVERITY_WARN, warnMessage, null));
    }

    public static void setFatalMessage(String fatalMessage, String id) {
        FacesContext.getCurrentInstance().addMessage(id,
                new FacesMessage(FacesMessage.SEVERITY_FATAL, fatalMessage, null));
    }

    public static void setErrorMessage(String errorMessage) {
        setErrorMessage(errorMessage, null);
    }

    public static void setInfoMessage(String errorMessage) {
        setInfoMessage(errorMessage, null);
    }

    public static void setWarnMessage(String errorMessage) {
        setWarnMessage(errorMessage, null);
    }

    public static void setFatalMessage(String errorMessage) {
        setFatalMessage(errorMessage, null);
    }

    public static void clearMessages() {
        FacesContext context = FacesContext.getCurrentInstance();
        Iterator<FacesMessage> it = context.getMessages();
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
    }
}
