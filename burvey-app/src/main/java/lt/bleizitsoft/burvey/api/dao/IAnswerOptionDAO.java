package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.AnswerOption;

import java.util.List;

/**
 * Created by marius on 5/23/17.
 */
public interface IAnswerOptionDAO
        extends IBaseDAO<AnswerOption> {

    List<AnswerOption> readByColumnContent(String content);
}
