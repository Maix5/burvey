package lt.bleizitsoft.burvey.api.staticobjects;

/**
 * Created by marius on 5/23/17.
 */
public enum SurveyQuestionType {
    TEXT_QUESTION,
    CHECKBOX_QUESTION,
    MULTIPLE_CHOICE_QUESTION,
    SCALE_QUESTION;

    public final static String text           = "TEXT";
    public final static String multipleChoice = "MULTIPLECHOICE";
    public final static String checkbox = "CHECKBOX";
    public final static String scale          = "SCALE";
    public final static String none           = "";

    /**
     *
     * @Warning this method definition collects leaked information from
     *  database (this means realization is thinly coupled with data base
     *  tables realization).
     */
    @Override
    public String toString() {
        switch (this){
            case TEXT_QUESTION: return text;
            case CHECKBOX_QUESTION: return multipleChoice;
            case MULTIPLE_CHOICE_QUESTION: return checkbox;
            case SCALE_QUESTION: return scale;
            default: return none;
        }
    }
}
