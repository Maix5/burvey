package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.UserAnswerNumber;

import java.util.List;

/**
 * Created by Monika.Kelpsaite on 5/14/2017.
 */
public abstract class AbstractUserAnswerNumberDAO
        extends AbstractBaseDAO<UserAnswerNumber>
        implements IUserAnswerNumberDAO {

    @Override
    public List<UserAnswerNumber> readByColumnContentNumber(
            Integer contentNumber) {
        return getSingleParamQueryEntityList(
                "UserAnswerNumber.findByContentNumber",
                "contentNumber", contentNumber);
    }

    @Override
    protected Class<UserAnswerNumber> getEntityClassType() {
        return UserAnswerNumber.class;
    }
    @Override
    protected String getReadAllQueryName() {
        return "UserAnswerNumber.findAll";
    }
}
