package lt.bleizitsoft.burvey.api.authentification;

import lt.bleizitsoft.burvey.entities.UserProfile;

import javax.enterprise.inject.Produces;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Optional;

/**
 * Created by marius on 5/23/17.
 */
public interface IAuthenticatedUserHolder extends Serializable {

    default Optional<UserProfile> getMaybeAuthenticatedUserProfile(){
        return Optional.ofNullable(getAuthenticatedUserProfile());
    }

    UserProfile getAuthenticatedUserProfile();

    void initUser(String userEmailAddress, String userPassword);

    void updateAuthenticatedUserProfile(String email);
    void updateAuthenticatedUserProfile(UserProfile userProfile);

    default void updateAuthenticatedUserProfile(){
        updateAuthenticatedUserProfile((UserProfile) null);
    }

    /**
     * @return
     *   Produces Authenticated user metadata
     *   can be accessed with @Inject.
     *   The same for entire user session.
     */
    @Produces
    @Named("authenticatedUser")
    @ProduceWhenLoggedIn
    default UserProfile produceAuthenticatedUserProfile(){
        return getAuthenticatedUserProfile();
    }

    default boolean isUser(){
        return getMaybeAuthenticatedUserProfile().isPresent()
                && !getAuthenticatedUserProfile().getBlocked();
    }

    default boolean isAdministrator(){
        return isUser()
                && getAuthenticatedUserProfile().getAdministrator();
    }

    default boolean isAnonymousUser(){
        return !getMaybeAuthenticatedUserProfile().isPresent();
    }
}
