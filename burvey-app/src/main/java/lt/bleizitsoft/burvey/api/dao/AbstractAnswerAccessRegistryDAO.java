package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.AnswersAccessRegistry;

import java.util.Optional;

/**
 * Created by Monika.Kelpsaite on 5/14/2017.
 */
public abstract class AbstractAnswerAccessRegistryDAO
        extends AbstractBaseDAO<AnswersAccessRegistry>
        implements IAnswerAccessRegistryDAO {

    @Override
    public Optional<AnswersAccessRegistry> readByColumnAccessLink(String accessLink) {
        return getQuerySingleEntity(
                getEm().createNamedQuery(
                        "AnswersAccessRegistry.findByAccessLink",
                        getEntityClassType()).setParameter("accessLink", accessLink));
    }


    @Override
    protected Class<AnswersAccessRegistry> getEntityClassType() {
        return AnswersAccessRegistry.class;
    }
    @Override
    protected String getReadAllQueryName() {
        return "AnswersAccessRegistry.findAll";
    }

    public AnswersAccessRegistry getAnswerAccessRegistryByLink(String accessLink) {
        return (AnswersAccessRegistry) getEm().createNamedQuery
                ("AnswersAccessRegistry.findByAccessLink",
                AnswersAccessRegistry.class).setParameter("accessLink", accessLink).getSingleResult();
    }
}
