package lt.bleizitsoft.burvey.api.emailService;

/**
 * Created by Monika.Kelpsaite on 5/19/2017.
 */
public interface ILinkEmailer {

    boolean sendLinkMail(String recipient,
                         String subject,
                         String emailHtmlPattern,
                         String surveyLink);

    boolean isValidEmailAddress(String email);
}
