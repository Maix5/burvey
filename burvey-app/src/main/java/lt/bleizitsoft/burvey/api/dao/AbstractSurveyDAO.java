package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.Survey;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by Monika.Kelpsaite on 5/7/2017.
 */
public abstract class AbstractSurveyDAO
        extends AbstractBaseDAO<Survey>
        implements ISurveyDAO {

    @Override
    public Optional<Survey> readByColumnAccessLink(String accessLink) {
        return getSingleParamQuerySingleEntity(
                "Survey.findByAccessLink", "accessLink", accessLink);
    }

    @Override
    public List<Survey> readByColumnTitle(String title) {
        return getSingleParamQueryEntityList(
                "Survey.findByTitle", "title", title);
    }

    @Override
    public List<Survey> readByColumnDescription(String description) {
        return getSingleParamQueryEntityList(
                "Survey.findByDescription", "description", description);
    }

    @Override
    public List<Survey> readByColumnActivationDate(Date activationDate) {
        return getSingleParamQueryEntityList(
                "Survey.findByActivationDate",
                "activationDate", activationDate);
    }

    @Override
    public List<Survey> readByColumnExpirationDate(Date expirationDate) {
        return getSingleParamQueryEntityList(
                "Survey.findByExpirationDate",
                "expirationDate", expirationDate);
    }

    @Override
    public List<Survey> readByColumnIsActive(Boolean isActive) {
        return getSingleParamQueryEntityList(
                "Survey.findByIsActive", "isActive", isActive);
    }

    @Override
    public List<Survey> readByColumnIsPublic(Boolean isPublic) {
        return getSingleParamQueryEntityList(
                "Survey.findByIsPublic", "isPublic", isPublic);
    }

    @Override
    public List<Survey> readByColumnQuestionsPerPage(Integer questionsPerPage) {
        return getSingleParamQueryEntityList(
                "Survey.findByQuestionsPerPage",
                "questionsPerPage", questionsPerPage);
    }


    @Override
    protected Class<Survey> getEntityClassType() {
        return Survey.class;
    }
    @Override
    protected String getReadAllQueryName() {
        return "Survey.findAll";
    }
}
