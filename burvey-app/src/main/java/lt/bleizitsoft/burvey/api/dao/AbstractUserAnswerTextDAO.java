package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.UserAnswerText;

import java.util.List;

/**
 * Created by Monika.Kelpsaite on 5/14/2017.
 */
public abstract class AbstractUserAnswerTextDAO
        extends AbstractBaseDAO<UserAnswerText>
        implements IUserAnswerTextDAO {

    @Override
    public List<UserAnswerText> readByColumnContentText(String contentText) {
        return getSingleParamQueryEntityList(
                "UserAnswerText.findByContentText", "contentText", contentText);
    }


    @Override
    protected Class<UserAnswerText> getEntityClassType() {
        return UserAnswerText.class;
    }
    @Override
    protected String getReadAllQueryName() {
        return "UserAnswerText.findAll";
    }
}
