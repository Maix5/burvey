package lt.bleizitsoft.burvey.api.dao;

import lt.bleizitsoft.burvey.entities.CandidateInfo;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Created by marius on 5/23/17.
 */
public interface ICandidateInfoDAO extends IBaseDAO<CandidateInfo> {

    Optional<CandidateInfo> readByColumnEmailAddress(String emailAddress);
    Optional<CandidateInfo> readByColumnPasswordRecoveryLink(
            String passwordRecoveryLink);
    Optional<CandidateInfo> readByColumnActivationLink(
            String activationLink);

    List<CandidateInfo> readByColumnActivationDate(Date activationDate);
    List<CandidateInfo> readByColumnActivationExpirationDate(
            Date activationExpirationDate);
}
