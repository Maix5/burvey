/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author marius
 */
@Entity
@Table(name = "candidate_info")
@NamedQueries({
    @NamedQuery(
            name = "CandidateInfo.findAll",
            query = "SELECT c FROM CandidateInfo c")
    , @NamedQuery(
            name = "CandidateInfo.findById",
            query = "SELECT c FROM CandidateInfo c WHERE c.id = :id")
    , @NamedQuery(
            name = "CandidateInfo.findByEmailAddress",
            query = "SELECT c " +
                    "FROM CandidateInfo c " +
                    "WHERE c.emailAddress = :emailAddress")
    , @NamedQuery(
            name = "CandidateInfo.findByPasswordRecoveryLink",
            query = "SELECT c " +
                    "FROM CandidateInfo c " +
                    "WHERE c.passwordRecoveryLink = :passwordRecoveryLink")
    , @NamedQuery(
            name = "CandidateInfo.findByActivationDate",
            query = "SELECT c " +
                    "FROM CandidateInfo c " +
                    "WHERE c.activationDate = :activationDate")
    , @NamedQuery(
            name = "CandidateInfo.findByActivationExpirationDate",
            query = "SELECT c " +
                    "FROM CandidateInfo c " +
                    "WHERE " +
                    "c.activationExpirationDate = :activationExpirationDate")
    , @NamedQuery(
            name = "CandidateInfo.findByActivationLink",
            query = "SELECT c " +
                    "FROM CandidateInfo c " +
                    "WHERE c.activationLink = :activationLink")
    , @NamedQuery(
            name = "CandidateInfo.findByOptLockVersion",
            query = "SELECT c " +
                    "FROM CandidateInfo c " +
                    "WHERE c.optLockVersion = :optLockVersion")})
@Getter
@Setter
@EqualsAndHashCode(of = {"emailAddress"})
@ToString(of = {"id", "emailAddress"})
public class CandidateInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "email_address")
    private String emailAddress;

    @Size(max = 511)
    @Column(name = "password_recovery_link")
    private String passwordRecoveryLink;

    @Column(name = "activation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date activationDate;

    @Column(name = "activation_expiration_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date activationExpirationDate;

    @Size(max = 511)
    @Column(name = "activation_link")
    private String activationLink;

    @Version
    @Column(name = "opt_lock_version")
    private Integer optLockVersion = 0;


    /**
     * Relationships:
     */
    //--------------------------------------------------------------------------
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "candidateInfo")
    private UserProfile userProfile;
    //--------------------------------------------------------------------------


    public CandidateInfo() {
    }

    public CandidateInfo(Integer id) {
        this.id = id;
    }

    public CandidateInfo(Integer id, String emailAddress) {
        this.id = id;
        this.emailAddress = emailAddress;
    }
}
