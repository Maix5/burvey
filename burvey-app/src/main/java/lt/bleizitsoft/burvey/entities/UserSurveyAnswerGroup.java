/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author marius
 */
@Entity
@Table(name = "user_survey_answer_group")
@NamedQueries({
    @NamedQuery(
            name = "UserSurveyAnswerGroup.findAll",
            query = "SELECT u FROM UserSurveyAnswerGroup u")
    , @NamedQuery(
            name = "UserSurveyAnswerGroup.findById",
            query = "SELECT u FROM UserSurveyAnswerGroup u WHERE u.id = :id")
    , @NamedQuery(
        name = "UserSurveyAnswerGroup.findByAccessRegistryId",
        query = "SELECT u FROM UserSurveyAnswerGroup u WHERE u.answersAccessRegistry = :answersAccessRegistry")
    , @NamedQuery(
            name = "UserSurveyAnswerGroup.findByOptLockVersion",
            query = "SELECT u " +
                    "FROM UserSurveyAnswerGroup u " +
                    "WHERE u.optLockVersion = :optLockVersion")})
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@ToString(of = {"id"})
public class UserSurveyAnswerGroup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Version
    @Column(name = "opt_lock_version")
    private int optLockVersion;


    /**
     * Relationships:
     */
    //--------------------------------------------------------------------------
    @JoinColumn(name = "answers_access_registry_id", referencedColumnName = "id")
    @OneToOne
    private AnswersAccessRegistry answersAccessRegistry;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userSurveyAnswerGroup")
    private List<UserAnswer> userAnswerList;
    //--------------------------------------------------------------------------


    public UserSurveyAnswerGroup() {
    }

    public UserSurveyAnswerGroup(Integer id) {
        this.id = id;
    }
}
