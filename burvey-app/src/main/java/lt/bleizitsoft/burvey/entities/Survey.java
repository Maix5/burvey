/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author marius
 */
@Entity
@Table(name = "survey")
@NamedQueries({
    @NamedQuery(
            name = "Survey.findAll",
            query = "SELECT s FROM Survey s")
    , @NamedQuery(
            name = "Survey.findById",
            query = "SELECT s FROM Survey s WHERE s.id = :id")
    , @NamedQuery(
            name = "Survey.findByAccessLink",
            query = "SELECT s FROM Survey s WHERE s.accessLink = :accessLink")
    , @NamedQuery(
            name = "Survey.findByTitle",
            query = "SELECT s FROM Survey s WHERE s.title = :title")
    , @NamedQuery(
            name = "Survey.findByDescription",
            query = "SELECT s " +
                    "FROM Survey s " +
                    "WHERE s.description = :description")
    , @NamedQuery(
            name = "Survey.findByOptLockVersion",
            query = "SELECT s FROM Survey s WHERE s.optLockVersion = :optLockVersion")
    , @NamedQuery(
            name = "Survey.findByActivationDate",
            query = "SELECT s " +
                    "FROM Survey s " +
                    "WHERE s.activationDate = :acctivationDate")
    , @NamedQuery(
            name = "Survey.findByExpirationDate",
            query = "SELECT s " +
                    "FROM Survey s " +
                    "WHERE s.expirationDate = :expirationDate")
    , @NamedQuery(
            name = "Survey.findByIsActive",
            query = "SELECT s FROM Survey s WHERE s.isActive = :isActive")
    , @NamedQuery(
            name = "Survey.findByIsPublic",
            query = "SELECT s FROM Survey s WHERE s.isPublic = :isPublic")
    , @NamedQuery(
            name = "Survey.findByQuestionsPerPage",
            query = "SELECT s " +
                    "FROM Survey s " +
                    "WHERE s.questionsPerPage = :questionsPerPage")})
@Getter
@Setter
@EqualsAndHashCode(of = {"accessLink"})
@ToString(of = {"id", "accessLink"})
public class Survey implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Size(max = 255)
    @Column(name = "access_link")
    private String accessLink;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "title")
    private String title;

    @Size(max = 1023)
    @Column(name = "description")
    private String description;

    @Version
    @Column(name = "opt_lock_version")
    private Integer optLockVersion = 0;

    @Column(name = "activation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date activationDate;

    @Column(name = "expiration_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;

    @NotNull
    @Column(name = "is_active")
    private Boolean isActive;

    @NotNull
    @Column(name = "is_public")
    private Boolean isPublic;

    @NotNull
    @Column(name = "questions_per_page")
    private Integer questionsPerPage;


    /**
     * Relationships:
     */
    //--------------------------------------------------------------------------
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "survey", fetch = FetchType.EAGER)
    private List<Question> questionList;

    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private UserProfile owner;
    //--------------------------------------------------------------------------

    public Survey() {
    }
    public Survey(Integer id) {
        this.id = id;
    }
    public Survey(Integer id, String title,
                  Boolean isActive, Boolean isPublic, Integer questionPerPage) {
        this.id = id;
        this.title = title;
        this.isActive = isActive;
        this.isPublic = isPublic;
        this.questionsPerPage = questionPerPage;
    }
}
