/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author marius
 */
@Entity
@Table(name = "answer_option")
@NamedQueries({
    @NamedQuery(
            name = "AnswerOption.findAll",
            query = "SELECT a FROM AnswerOption a")
    , @NamedQuery(
            name = "AnswerOption.findById",
            query = "SELECT a FROM AnswerOption a WHERE a.id = :id")
    , @NamedQuery(
            name = "AnswerOption.findByContent",
            query = "SELECT a FROM AnswerOption a WHERE a.content = :content")
    , @NamedQuery(
            name = "AnswerOption.findByOptLockVersion",
            query = "SELECT a " +
                    "FROM AnswerOption a " +
                    "WHERE a.optLockVersion = :optLockVersion")})
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@ToString(of = {"id", "content"})
public class AnswerOption implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 127)
    @Column(name = "content")
    private String content;

    @Version
    @Column(name = "opt_lock_version")
    private Integer optLockVersion = 0;


    /**
     * Relationships:
     */
    //--------------------------------------------------------------------------
    @JoinTable(name = "multiple_choice_answer_binding",
            joinColumns = {
                    @JoinColumn(name = "answer_option_id",
                            referencedColumnName = "id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "user_multiple_choice_answer_option_id",
                            referencedColumnName = "id")})
    @ManyToMany
    private List<UserMultipleChoiceAnswerOption>
            userMultipleChoiceAnswerOptionList;

    @JoinColumn(name = "question_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Question question;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "answerOption")
    private List<UserSingleAnswerOption> userSingleAnswerOptionList;
    //--------------------------------------------------------------------------


    public AnswerOption() {
    }

    public AnswerOption(Integer id) {
        this.id = id;
    }

    public AnswerOption(Integer id, String content) {
        this.id = id;
        this.content = content;
    }
}
