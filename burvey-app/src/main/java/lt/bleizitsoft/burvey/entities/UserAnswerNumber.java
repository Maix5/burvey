/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 *
 * @author marius
 */
@Entity
@Table(name = "user_answer_number")
@NamedQueries({
    @NamedQuery(
            name = "UserAnswerNumber.findAll",
            query = "SELECT u FROM UserAnswerNumber u")
    , @NamedQuery(
            name = "UserAnswerNumber.findById",
            query = "SELECT u FROM UserAnswerNumber u WHERE u.id = :id")
    , @NamedQuery(
            name = "UserAnswerNumber.findByContentNumber",
            query = "SELECT u " +
                    "FROM UserAnswerNumber u " +
                    "WHERE u.contentNumber = :contentNumber")
    , @NamedQuery(
            name = "UserAnswerNumber.findByOptLockVersion",
            query = "SELECT u " +
                    "FROM UserAnswerNumber u " +
                    "WHERE u.optLockVersion = :optLockVersion")})
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@ToString(of = {"id", "contentNumber"})
public class UserAnswerNumber implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "content_number")
    private Integer contentNumber;

    @Version
    @Column(name = "opt_lock_version")
    private Integer optLockVersion = 0;


    /**
     * Relationships:
     */
    //--------------------------------------------------------------------------
    @JoinColumn(name = "user_answer_id", referencedColumnName = "id")
    @OneToOne(optional = false)
    private UserAnswer userAnswer;
    //--------------------------------------------------------------------------

    public UserAnswerNumber() {
    }

    public UserAnswerNumber(Integer id) {
        this.id = id;
    }

    public UserAnswerNumber(Integer id, int contentNumber) {
        this.id = id;
        this.contentNumber = contentNumber;
    }
}
