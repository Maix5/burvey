/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 *
 * @author marius
 */
@Entity
@Table(name = "user_answer_text")
@NamedQueries({
    @NamedQuery(
            name = "UserAnswerText.findAll",
            query = "SELECT u FROM UserAnswerText u")
    , @NamedQuery(
            name = "UserAnswerText.findById",
            query = "SELECT u FROM UserAnswerText u WHERE u.id = :id")
    , @NamedQuery(
            name = "UserAnswerText.findByContentText",
            query = "SELECT u " +
                    "FROM UserAnswerText u " +
                    "WHERE u.contentText = :contentText")
    , @NamedQuery(
            name = "UserAnswerText.findByOptLockVersion",
            query = "SELECT u " +
                    "FROM UserAnswerText u " +
                    "WHERE u.optLockVersion = :optLockVersion")})
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@ToString(of = {"id", "contentText"})
public class UserAnswerText implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Size(max = 8191)
    @Column(name = "content_text")
    private String contentText;

    @Version
    @Column(name = "opt_lock_version")
    private Integer optLockVersion = 0;


    /**
     * Relationships:
     */
    //--------------------------------------------------------------------------
    @JoinColumn(name = "user_answer_id", referencedColumnName = "id")
    @OneToOne(optional = false)
    private UserAnswer userAnswer;
    //--------------------------------------------------------------------------


    public UserAnswerText() {
    }

    public UserAnswerText(Integer id) {
        this.id = id;
    }
}
