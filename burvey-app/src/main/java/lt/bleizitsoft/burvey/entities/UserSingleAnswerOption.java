/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author marius
 */
@Entity
@Table(name = "user_single_answer_option")
@NamedQueries({
    @NamedQuery(
            name = "UserSingleAnswerOption.findAll",
            query = "SELECT u FROM UserSingleAnswerOption u")
    , @NamedQuery(
            name = "UserSingleAnswerOption.findById",
            query = "SELECT u FROM UserSingleAnswerOption u WHERE u.id = :id")
    , @NamedQuery(
            name = "UserSingleAnswerOption.findByOptLockVersion",
            query = "SELECT u " +
                    "FROM UserSingleAnswerOption u " +
                    "WHERE u.optLockVersion = :optLockVersion")})
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@ToString(of = {"id"})
public class UserSingleAnswerOption implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Version
    @Column(name = "opt_lock_version")
    private int optLockVersion;

    /**
     * Relationships:
     */
    //--------------------------------------------------------------------------
    @JoinColumn(name = "answer_option_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private AnswerOption answerOption;

    @JoinColumn(name = "user_answer_id", referencedColumnName = "id")
    @OneToOne(optional = false)
    private UserAnswer userAnswer;
    //--------------------------------------------------------------------------

    public UserSingleAnswerOption() {
    }

    public UserSingleAnswerOption(Integer id) {
        this.id = id;
    }
}
