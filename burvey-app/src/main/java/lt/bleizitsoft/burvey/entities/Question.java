/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author marius
 */
@Entity
@Table(name = "question")
@NamedQueries({
    @NamedQuery(
            name = "Question.findAll",
            query = "SELECT q FROM Question q")
    , @NamedQuery(
            name = "Question.findById",
            query = "SELECT q FROM Question q WHERE q.id = :id")
    , @NamedQuery(
            name = "Question.findBySurveyId",
            query = "SELECT q FROM Question q WHERE q.survey.id = :surveyId")
    , @NamedQuery(
            name = "Question.findByQuestionType",
            query = "SELECT q " +
                    "FROM Question q " +
                    "WHERE q.questionType = :questionType")
    , @NamedQuery(
            name = "Question.findByContent",
            query = "SELECT q FROM Question q WHERE q.content = :content")
    , @NamedQuery(
            name = "Question.findByOrderNr",
            query = "SELECT q FROM Question q WHERE q.orderNr = :orderNr")
    , @NamedQuery(
            name = "Question.findByRequired",
            query = "SELECT q FROM Question q WHERE q.required = :required")
    , @NamedQuery(
            name = "Question.findByOptLockVersion",
            query = "SELECT q " +
                    "FROM Question q " +
                    "WHERE q.optLockVersion = :optLockVersion")})
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@ToString(of = {"id"})
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "question_type")
    private String questionType;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4096)
    @Column(name = "content")
    private String content;

    @Basic(optional = false)
    @NotNull
    @Column(name = "order_nr")
    private Integer orderNr;

    @Column(name = "required")
    private Boolean required;

    @Version
    @Column(name = "opt_lock_version")
    private Integer optLockVersion = 0;


    /**
     * Relationships:
     */
    //--------------------------------------------------------------------------
    @JoinColumn(name = "survey_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Survey survey;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "question", fetch = FetchType.EAGER)
    private List<AnswerOption> answerOptionList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "question")
    private List<UserAnswer> userAnswerList;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "question")
    private NumberQuestion numberQuestion;
    //--------------------------------------------------------------------------


    public Question() {
    }

    public Question(Integer id) {
        this.id = id;
    }

    public Question(Integer id, String questionType, String content,
                    int orderNr) {
        this.id = id;
        this.questionType = questionType;
        this.content = content;
        this.orderNr = orderNr;
    }
}
