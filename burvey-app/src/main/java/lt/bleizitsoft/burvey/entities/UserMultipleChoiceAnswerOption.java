/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author marius
 */
@Entity
@Table(name = "user_multiple_choice_answer_option")
@NamedQueries({
    @NamedQuery(
            name = "UserMultipleChoiceAnswerOption.findAll",
            query = "SELECT u FROM UserMultipleChoiceAnswerOption u")
    , @NamedQuery(
            name = "UserMultipleChoiceAnswerOption.findById",
            query = "SELECT u " +
                    "FROM UserMultipleChoiceAnswerOption u " +
                    "WHERE u.id = :id")
    , @NamedQuery(
            name = "UserMultipleChoiceAnswerOption.findByOptLockVersion",
            query = "SELECT u " +
                    "FROM UserMultipleChoiceAnswerOption u " +
                    "WHERE u.optLockVersion = :optLockVersion")})
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@ToString(of = {"id"})
public class UserMultipleChoiceAnswerOption implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Version
    @Column(name = "opt_lock_version")
    private int optLockVersion;


    /**
     * Relationships:
     */
    //--------------------------------------------------------------------------

    @ManyToMany(mappedBy = "userMultipleChoiceAnswerOptionList")
    private List<AnswerOption> answerOptionList;

    @JoinColumn(name = "user_answer_id", referencedColumnName = "id")
    @OneToOne(optional = false)
    private UserAnswer userAnswer;
    //--------------------------------------------------------------------------

    public UserMultipleChoiceAnswerOption() {
    }

    public UserMultipleChoiceAnswerOption(Integer id) {
        this.id = id;
    }
}
