/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 *
 * @author marius
 */
@Entity
@Table(name = "answers_access_registry")
@NamedQueries({
    @NamedQuery(
            name = "AnswersAccessRegistry.findAll",
            query = "SELECT a FROM AnswersAccessRegistry a")
    , @NamedQuery(
            name = "AnswersAccessRegistry.findById",
            query = "SELECT a " +
                    "FROM AnswersAccessRegistry a " +
                    "WHERE a.id = :id")
    , @NamedQuery(
            name = "AnswersAccessRegistry.findByAccessLink",
            query = "SELECT a " +
                    "FROM AnswersAccessRegistry a " +
                    "WHERE a.accessLink = :accessLink")
    , @NamedQuery(
            name = "AnswersAccessRegistry.findByOptLockVersion",
            query = "SELECT a " +
                    "FROM AnswersAccessRegistry a " +
                    "WHERE a.optLockVersion = :optLockVersion")})
@Getter
@Setter
@EqualsAndHashCode(of = {"accessLink"})
@ToString(of = {"id", "accessLink"})
public class AnswersAccessRegistry implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Size(max = 255)
    @Column(name = "access_link")
    private String accessLink;

    @Version
    @Column(name = "opt_lock_version")
    private Integer optLockVersion = 0;


    /**
     * Relationships:
     */
    //--------------------------------------------------------------------------
    @OneToOne(mappedBy = "answersAccessRegistry")
    private UserSurveyAnswerGroup userSurveyAnswerGroup;
    //--------------------------------------------------------------------------


    public AnswersAccessRegistry() {
    }

    public AnswersAccessRegistry(Integer id) {
        this.id = id;
    }
}
