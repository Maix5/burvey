/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author marius
 */
@Entity
@Table(name = "user_profile")
@NamedQueries({
    @NamedQuery(
            name = "UserProfile.findAll",
            query = "SELECT u FROM UserProfile u")
    , @NamedQuery(
            name = "UserProfile.findById",
            query = "SELECT u FROM UserProfile u WHERE u.id = :id")
    , @NamedQuery(
            name = "UserProfile.findByName",
            query = "SELECT u FROM UserProfile u WHERE u.name = :name")
    , @NamedQuery(
            name = "UserProfile.findBySurname",
            query = "SELECT u FROM UserProfile u WHERE u.surname = :surname")
    , @NamedQuery(
            name = "UserProfile.findByNameSurname",
            query = "SELECT u " +
                    "FROM UserProfile u " +
                    "WHERE u.name = :name AND u.surname = :surname")
    , @NamedQuery(
            name = "UserProfile.findByHashedPassword",
            query = "SELECT u " +
                    "FROM UserProfile u " +
                    "WHERE u.hashedPassword = :hashedPassword")
    , @NamedQuery(
            name = "UserProfile.findByAdministrator",
            query = "SELECT u " +
                    "FROM UserProfile u " +
                    "WHERE u.administrator = :administrator")
    , @NamedQuery(
            name = "UserProfile.findByBlocked",
            query = "SELECT u " +
                    "FROM UserProfile u " +
                    "WHERE u.blocked = :blocked")
    , @NamedQuery(
            name = "UserProfile.findByOptLockVersion",
            query = "SELECT u " +
                    "FROM UserProfile u " +
                    "WHERE u.optLockVersion = :optLockVersion")})
@Getter
@Setter
@EqualsAndHashCode(of = {"id", "name", "surname"})
@ToString(of = {"id", "name", "surname"})
public class UserProfile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "name")
    private String name;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 31)
    @Column(name = "surname")
    private String surname;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "hashed_password")
    private String hashedPassword;

    @Column(name = "administrator")
    private Boolean administrator;

    @Column(name = "blocked")
    private Boolean blocked;

    @Version
    @Column(name = "opt_lock_version")
    private Integer optLockVersion = 0;


    /**
     * Relationships:
     */
    //--------------------------------------------------------------------------
    @JoinColumn(name = "candidate_info_id", referencedColumnName = "id")
    @OneToOne(optional = false)
    private CandidateInfo candidateInfo;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "owner")
    private List<Survey> surveyList;
    //--------------------------------------------------------------------------

    public UserProfile() {
    }

    public UserProfile(Integer id) {
        this.id = id;
    }

    public UserProfile(Integer id, String name, String surname,
                       String hashedPassword) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.hashedPassword = hashedPassword;
    }
}
