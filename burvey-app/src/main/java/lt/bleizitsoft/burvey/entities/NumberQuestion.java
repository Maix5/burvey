/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author marius
 */
@Entity
@Table(name = "number_question")
@NamedQueries({
    @NamedQuery(
            name = "NumberQuestion.findAll",
            query = "SELECT n FROM NumberQuestion n")
    , @NamedQuery(
            name = "NumberQuestion.findById",
            query = "SELECT n FROM NumberQuestion n WHERE n.id = :id")
    , @NamedQuery(
            name = "NumberQuestion.findByMinimalRange",
            query = "SELECT n " +
                    "FROM NumberQuestion n " +
                    "WHERE n.minimalRange = :minimalRange")
    , @NamedQuery(
            name = "NumberQuestion.findByMaximumRange",
            query = "SELECT n " +
                    "FROM NumberQuestion n " +
                    "WHERE n.maximumRange = :maximumRange")
    , @NamedQuery(
            name = "NumberQuestion.findByOptLockVersion",
            query = "SELECT n " +
                    "FROM NumberQuestion n " +
                    "WHERE n.optLockVersion = :optLockVersion")})
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@ToString(of = {"id"})
public class NumberQuestion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "minimal_range")
    private Integer minimalRange;

    @Column(name = "maximum_range")
    private Integer maximumRange;

    @Version
    @Column(name = "opt_lock_version")
    private Integer optLockVersion = 0;


    /**
     * Relationships:
     */
    //--------------------------------------------------------------------------
    @JoinColumn(name = "question_id", referencedColumnName = "id")
    @OneToOne(optional = false)
    private Question question;
    //--------------------------------------------------------------------------


    public NumberQuestion() {
    }
    public NumberQuestion(Integer id) {
        this.id = id;
    }
}
