/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.bleizitsoft.burvey.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

/**
 *
 * @author marius
 */
@Entity
@Table(name = "user_answer")
@NamedQueries({
    @NamedQuery(
            name = "UserAnswer.findAll",
            query = "SELECT u FROM UserAnswer u")
    , @NamedQuery(
            name = "UserAnswer.findById",
            query = "SELECT u FROM UserAnswer u WHERE u.id = :id")
    , @NamedQuery(
            name = "UserAnswer.findByOptLockVersion",
            query = "SELECT u " +
                    "FROM UserAnswer u " +
                    "WHERE u.optLockVersion = :optLockVersion")})
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@ToString(of = {"id"})
public class UserAnswer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Version
    @Column(name = "opt_lock_version")
    private Integer optLockVersion = 0;


    /**
     * Relationships:
     */
    //--------------------------------------------------------------------------
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userAnswer")
    private UserAnswerNumber userAnswerNumber;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userAnswer")
    private UserMultipleChoiceAnswerOption userMultipleChoiceAnswerOption;

    @JoinColumn(name = "question_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Question question;

    @JoinColumn(name = "user_survey_answer_group_id",
                referencedColumnName = "id")
    @ManyToOne(optional = false, cascade=CascadeType.ALL)
    private UserSurveyAnswerGroup userSurveyAnswerGroup;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userAnswer")
    private UserSingleAnswerOption userSingleAnswerOption;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userAnswer")
    private UserAnswerText userAnswerText;
    //--------------------------------------------------------------------------

    public UserAnswer() {
    }

    public UserAnswer(Integer id) {
        this.id = id;
    }
}
