-- Create table sentences for burvey database

CREATE TABLE IF NOT EXISTS burvey.candidate_info (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    email_address VARCHAR(255) NOT NULL,
    password_recovery_link VARCHAR(511),
    activation_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    activation_expiration_date DATETIME,
    activation_link VARCHAR(511),
    opt_lock_version INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (id),

    -- CONSTRAINT check_email CHECK (email like '%_@__%.__%'),-- Basic email check
    CONSTRAINT check_candidate_activation_date
        CHECK(activation_date >= CURRENT_DATE),
    CONSTRAINT check_candidate_expiration_date
        CHECK (activation_expiration_date >= CURRENT_DATE),
    CONSTRAINT check_date_bounds
        CHECK (activation_expiration_date >= activation_date)
);

CREATE TABLE IF NOT EXISTS burvey.user_profile (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    name CHAR(31) NOT NULL,
    surname CHAR(31) NOT NULL,
    hashed_password VARCHAR(255) NOT NULL,
    administrator TINYINT(1) DEFAULT 0,
    candidate_info_id INTEGER UNSIGNED NOT NULL,
    blocked TINYINT(1) DEFAULT 0,
    opt_lock_version INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (id),

    CONSTRAINT fk_to_candidate_info
        FOREIGN KEY (candidate_info_id)
            REFERENCES burvey.candidate_info (id)
            ON DELETE RESTRICT
            ON UPDATE RESTRICT
);

CREATE TABLE IF NOT EXISTS burvey.survey (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    access_link VARCHAR(255),
    title VARCHAR(255) NOT NULL,
    description VARCHAR(1023),
    owner_id INTEGER UNSIGNED NOT NULL,
    opt_lock_version INTEGER NOT NULL DEFAULT 0,
    activation_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    expiration_date DATETIME,
    is_active TINYINT(1) NOT NULL DEFAULT 0,
    is_public TINYINT(1) NOT NULL DEFAULT 0,
    questions_per_page INTEGER NOT NULL DEFAULT 0,


    PRIMARY KEY (id),

    CONSTRAINT fk_to_user_profile
        FOREIGN KEY (owner_id)
            REFERENCES burvey.user_profile(id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT check_survey_activation_date
        CHECK (activation_date >= CURRENT_DATE),
    CONSTRAINT check_survey_questions_per_page
        CHECK (questions_per_page >= 0)
);

CREATE TABLE IF NOT EXISTS burvey.question (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    question_type VARCHAR(31) NOT NULL DEFAULT 'TEXT',
    survey_id INTEGER UNSIGNED NOT NULL,
    content VARCHAR(4096) NOT NULL,
    order_nr INTEGER NOT NULL,
    required TINYINT(1) DEFAULT 0,
    opt_lock_version INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (id),

    CONSTRAINT question_type_definition
        CHECK(question_type
            IN ('TEXT', 'CHECKBOX', 'MULTIPLECHOICE', 'SCALE')),

    CONSTRAINT fk_to_survey_from_question
        FOREIGN KEY (survey_id)
            REFERENCES burvey.survey(id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS burvey.answers_access_registry (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    access_link VARCHAR(255),
    opt_lock_version INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS burvey.user_survey_answer_group (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    answers_access_registry_id INTEGER UNSIGNED,
    opt_lock_version INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (id),

    CONSTRAINT fk_to_answers_access_registry
        FOREIGN KEY (answers_access_registry_id)
            REFERENCES burvey.answers_access_registry(id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);


CREATE TABLE IF NOT EXISTS burvey.user_answer (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    question_id INTEGER UNSIGNED NOT NULL,
    user_survey_answer_group_id INTEGER UNSIGNED NOT NULL,
    opt_lock_version INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (id),

    CONSTRAINT fk_to_question_from_user_answer
        FOREIGN KEY (question_id)
            REFERENCES burvey.question(id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT fk_to_user_survey_answers_group_from_user_answer
        FOREIGN KEY (user_survey_answer_group_id)
            REFERENCES burvey.user_survey_answer_group(id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS burvey.user_answer_text (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    content_text VARCHAR(8191),
    opt_lock_version INTEGER NOT NULL DEFAULT 0,
    user_answer_id INTEGER UNSIGNED NOT NULL,

    PRIMARY KEY (id),

    CONSTRAINT fk_to_user_answer_from_user_answer_text
        FOREIGN KEY (user_answer_id)
            REFERENCES burvey.user_answer(id)
                ON DELETE CASCADE
                ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS burvey.answer_option (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    content VARCHAR(127) NOT NULL,
    question_id INTEGER UNSIGNED NOT NULL,
    opt_lock_version INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (id),

    CONSTRAINT fk_to_question_from_answer_option
        FOREIGN KEY (question_id)
            REFERENCES burvey.question(id)
                ON DELETE CASCADE
                ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS burvey.user_single_answer_option (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    answer_option_id INTEGER UNSIGNED NOT NULL,
    user_answer_id INTEGER UNSIGNED NOT NULL,
    opt_lock_version INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (id),

    CONSTRAINT fk_to_user_answer_from_user_single_answer_option
        FOREIGN KEY (user_answer_id)
            REFERENCES burvey.user_answer(id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT fk_to_answer_option_from_user_single_answer_option
        FOREIGN KEY (answer_option_id)
            REFERENCES burvey.answer_option(id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS burvey.user_multiple_choice_answer_option (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    user_answer_id INTEGER UNSIGNED NOT NULL,
    opt_lock_version INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (id),

    CONSTRAINT fk_to_user_answer_from_user_multiple_choice_answer_option
        FOREIGN KEY (user_answer_id)
            REFERENCES burvey.user_answer(id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS burvey.multiple_choice_answer_binding (
    answer_option_id INTEGER UNSIGNED NOT NULL,
    user_multiple_choice_answer_option_id INTEGER UNSIGNED NOT NULL,

    PRIMARY KEY (answer_option_id, user_multiple_choice_answer_option_id),

    CONSTRAINT fk_to_answer_option_from_user_multiple_choice_binding
        FOREIGN KEY (answer_option_id)
            REFERENCES burvey.answer_option(id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,
    CONSTRAINT fk_to_user_multiple_choice_answer_option_from_binding
        FOREIGN KEY (user_multiple_choice_answer_option_id)
            REFERENCES burvey.user_multiple_choice_answer_option(id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS burvey.number_question (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    minimal_range INTEGER,
    maximum_range INTEGER,
    question_id INTEGER UNSIGNED NOT NULL,
    opt_lock_version INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY (id),

    CONSTRAINT check_number_range
        CHECK(minimal_range <= maximum_range),

    CONSTRAINT fk_to_question_from_number_question
        FOREIGN KEY (question_id)
            REFERENCES burvey.question(id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS burvey.user_answer_number (
    id INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
    content_number INTEGER NOT NULL,
    opt_lock_version INTEGER NOT NULL DEFAULT 0,
    user_answer_id INTEGER UNSIGNED NOT NULL,

    PRIMARY KEY (id),

    CONSTRAINT fk_to_user_answer_from_user_answer_number
        FOREIGN KEY (user_answer_id)
            REFERENCES burvey.user_answer(id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);
# CREATE TABLE IF NOT EXISTS burvey.registered_user_answers (
#     answers_access_registry_id INTEGER UNSIGNED NOT NULL,
#     user_answer_id INTEGER UNSIGNED NOT NULL,
#     opt_lock_version INTEGER NOT NULL DEFAULT 0,
#
#     PRIMARY KEY (answers_access_registry_id, user_answer_id),
#
#     CONSTRAINT fk_to_answers_access_registry
#         FOREIGN KEY (answers_access_registry_id)
#             REFERENCES burvey.answers_access_registry(id)
#             ON DELETE CASCADE
#             ON UPDATE CASCADE,
#     CONSTRAINT fk_to_user_answer_from_registered_user_answers
#         FOREIGN KEY (user_answer_id)
#             REFERENCES burvey.user_answer(id)
#             ON DELETE CASCADE
#             ON UPDATE CASCADE
# );
