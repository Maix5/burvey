-- Create index sentences for burvey database

CREATE UNIQUE INDEX index_candidate_info_for_email_address
    ON burvey.candidate_info (email_address);
CREATE UNIQUE INDEX index_candidate_info_for_activation_link
    ON burvey.candidate_info (activation_link);
CREATE UNIQUE INDEX index_candidate_info_for_password_recovery_link
    ON burvey.candidate_info (password_recovery_link);

CREATE INDEX index_user_profile_for_name_surname
    ON burvey.user_profile (name, surname);

CREATE UNIQUE INDEX index_survey_for_access_link
    ON burvey.survey (access_link);
CREATE INDEX index_survey_for_title ON burvey.survey (title);

CREATE INDEX index_question_for_question_type
    ON burvey.question (question_type);
CREATE INDEX index_question_for_order_nr
    ON burvey.question (order_nr);

CREATE INDEX index_number_question_for_answer_range
    ON burvey.number_question (minimal_range, maximum_range);

CREATE UNIQUE INDEX index_answers_access_registry_for_access_link
    ON burvey.answers_access_registry (access_link);
