-- Create user sentences for burvey database

-- Creates users accounts: (password can be changed accordingly)
CREATE USER 'burvey-user'@'localhost' IDENTIFIED BY 'burvey-user';
# CREATE USER 'registered_user'@'localhost' IDENTIFIED BY 'registered';
# CREATE USER 'anonymous_user'@'localhost' IDENTIFIED BY 'anonymous';

-- Granting privileges to the administrator user:
GRANT CREATE TEMPORARY TABLES ON burvey.* TO 'burvey-user'@'localhost';

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP,
        INDEX, ALTER, CREATE TEMPORARY TABLES
    ON burvey.* TO 'burvey-user'@'localhost';

# -- Granting privileges to the registered user:
# GRANT CREATE TEMPORARY TABLES ON burvey.* TO 'registered_user'@'localhost';
#
# GRANT SELECT (id, email_address)
#     ON burvey.candidate_info TO 'registered_user'@'localhost';
# GRANT SELECT,
#         INSERT (name, surname, hashed_password, candidate_info_id),
#         UPDATE (name, surname, hashed_password, candidate_info_id),
#         DELETE
#     ON burvey.user_profile TO 'registered_user'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.survey TO 'registered_user'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.question TO 'registered_user'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.user_answer TO 'registered_user'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.user_answer_text TO 'registered_user'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.user_answer_option TO 'registered_user'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.user_answer_number TO 'registered_user'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.number_question TO 'registered_user'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.answer_option TO 'registered_user'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.answers_access_registry TO 'registered_user'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.registered_user_answers TO 'registered_user'@'localhost';
#
#
# -- Granting privileges to the anonymous user:
# GRANT CREATE TEMPORARY TABLES ON burvey.* TO 'anonymous_user'@'localhost';
#
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.user_answer TO 'anonymous_user'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.user_answer_text TO 'anonymous_user'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.user_answer_option TO 'anonymous_user'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.user_answer_number TO 'anonymous_user'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.answers_access_registry TO 'anonymous_user'@'localhost';
# GRANT SELECT, INSERT, UPDATE, DELETE
#     ON burvey.registered_user_answers TO 'anonymous_user'@'localhost';

FLUSH PRIVILEGES;
