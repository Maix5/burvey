-- Destroy index sentences for burvey database

DROP INDEX index_candidate_info_for_email_address
    ON burvey.candidate_info;
DROP INDEX index_candidate_info_for_actication_link
    ON burvey.candidate_info;
DROP INDEX index_candidate_info_for_password_recovery_link
    ON burvey.candidate_info;

DROP INDEX index_user_profile_for_name_surname ON burvey.user_profile;

DROP INDEX index_survey_for_access_link ON burvey.survey;
DROP INDEX index_survey_for_title ON burvey.survey;

DROP INDEX index_question_for_question_type ON burvey.question;
DROP INDEX index_question_for_order_nr ON burvey.question;

DROP INDEX index_number_question_for_answer_range ON burvey.number_question;

DROP INDEX index_answers_access_registry_for_access_link
    ON burvey.answers_access_registry;
