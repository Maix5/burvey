#!/bin/bash

# This script creates data base "burvey" and inicializes it with
# (tables, test data and so on....).

# Dependencies: mysql (server and client) with root access.
# Debian package "mysql-server".

mysql -u root -p < create_schema.sql

cat create_table.sql create_user.sql\
    create_index.sql create_trigger.sql\
    insert_test_data.sql\
    create_view.sql\
    > create_all.sql

# mysql -u root -p burvey < create_all.sql

