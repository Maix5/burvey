-- Insert test data sentences for burvey database

-- candidate_info
INSERT INTO burvey.candidate_info (id, email_address)
    VALUES(1, 'admin@admin.com');

-- user_profile
-- Root user: admin@admin.com, pass: admin
INSERT INTO burvey.user_profile
    (id, name, surname, hashed_password, administrator, candidate_info_id)
    VALUES(1,'admin', 'ADMIN',
           '$2a$10$XvkSaMOVDl43IeAYpJBEJO0O87VJydOzQyUF5dZkBddCrSdphKM06',
           1, 1);
