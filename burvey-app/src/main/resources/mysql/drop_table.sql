-- Destroy table sentences for burvey database

DROP TABLE IF EXISTS burvey.user_answer_number;
DROP TABLE IF EXISTS burvey.number_question;
DROP TABLE IF EXISTS burvey.multiple_choice_answer_binding;
DROP TABLE IF EXISTS burvey.user_multiple_choice_answer_option;
DROP TABLE IF EXISTS burvey.user_single_answer_option;
DROP TABLE IF EXISTS burvey.user_answer_text;
DROP TABLE IF EXISTS burvey.user_answer;
DROP TABLE IF EXISTS burvey.user_survey_answer_group;
DROP TABLE IF EXISTS burvey.answers_access_registry;
DROP TABLE IF EXISTS burvey.question;
DROP TABLE IF EXISTS burvey.survey;
DROP TABLE IF EXISTS burvey.user_profile;
DROP TABLE IF EXISTS burvey.candidate_info;