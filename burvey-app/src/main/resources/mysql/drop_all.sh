#!/bin/bash

# This script removes all components (tables, test data and so on....) in
# "burvey" database and drops the database itself.

# Dependencies: mysql (server and client) with root access.
# Debian package "mysql-server".

cat drop_user.sql drop_schema.sql > drop_all.sql

mysql -u root -p < drop_all.sql

